LOCUS       KP407051               16572 bp    DNA     circular PRI 31-DEC-2015
DEFINITION  Homo sapiens isolate C5S922814 mitochondrion, complete genome.
ACCESSION   KP407051
VERSION     KP407051.1  GI:883741105
KEYWORDS    .
SOURCE      mitochondrion Homo sapiens (human)
  ORGANISM  Homo sapiens
            Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
            Mammalia; Eutheria; Euarchontoglires; Primates; Haplorrhini;
            Catarrhini; Hominidae; Homo.
REFERENCE   1  (bases 1 to 16572)
  AUTHORS   Gandini,F., Achilli,A., Pala,M., Bodner,M., Brandini,S., Huber,G.,
            Egyed,B., Ferretti,L., Salas,A., Scozzari,R., Cruciani,F.,
            Coppa,A., Parson,W., Semino,O., Richards,M., Torroni,A. and
            Olivieri,A.
  TITLE     Early Holocene population movements from the Arabian Peninsula to
            the Horn of Africa marked by R0a mitogenomes
  JOURNAL   Unpublished
REFERENCE   2  (bases 1 to 16572)
  AUTHORS   Gandini,F., Achilli,A., Pala,M., Bodner,M., Brandini,S., Huber,G.,
            Egyed,B., Ferretti,L., Salas,A., Scozzari,R., Cruciani,F.,
            Coppa,A., Parson,W., Semino,O., Richards,M., Torroni,A. and
            Olivieri,A.
  TITLE     Direct Submission
  JOURNAL   Submitted (07-JAN-2015) Department of Biology and Biotechnology L.
            Spallanzani, University of Pavia, via Ferrata 1, Pavia, PV 27100,
            Italy
COMMENT     ##Assembly-Data-START##
            Assembly Method       :: Sequencher v. 5.0
            Sequencing Technology :: Sanger dideoxy sequencing; Illumina
            ##Assembly-Data-END##
FEATURES             Location/Qualifiers
     source          1..16572
                     /organism="Homo sapiens"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /isolate="C5S922814"
                     /isolation_source="Palestine, Gaza"
                     /db_xref="taxon:9606"
                     /haplotype="R0a2b2"
     D-loop          complement(join(16031..16572,1..581))
     tRNA            581..651
                     /product="tRNA-Phe"
     rRNA            652..1605
                     /product="12S ribosomal RNA"
     tRNA            1606..1674
                     /product="tRNA-Val"
     rRNA            1675..3232
                     /product="16S ribosomal RNA"
     tRNA            3233..3307
                     /product="tRNA-Leu"
                     /note="codons recognized: UUR"
     gene            3310..4265
                     /gene="ND1"
     CDS             3310..4265
                     /gene="ND1"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:4264..4265,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 1"
                     /protein_id="AKQ20249.1"
                     /db_xref="GI:883741106"
                     /translation="MPMANLLLLIVPILIAMAFLMLTERKILGYMQLRKGPNVVGPYG
                     LLQPFADAMKLFTKEPLKPATSTITLYITAPTLALTIALLLWTPLPMPNPLVNLNLGL
                     LFILATSSLAVYSILWSGWASNSNYALIGALRAVAQTISYEVTLAIILLSTLLMSGSF
                     NLSTLITTQEHLWLLLPSWPLAMMWFISTLAETNRTPFDLAEGESELVSGFNIEYAAG
                     PFALFFMAEYTNIIMMNTLTTTIFLGTTYDALSPELYTTYFVTKTLLLTSLFLWIRTA
                     YPRFRYDQLMHLLWKNFLPLTLALLMWYVSMPITISSIPPQT"
     tRNA            4266..4334
                     /product="tRNA-Ile"
     tRNA            complement(4332..4403)
                     /product="tRNA-Gln"
     tRNA            4405..4472
                     /product="tRNA-Met"
     gene            4473..5514
                     /gene="ND2"
     CDS             4473..5514
                     /gene="ND2"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:5514,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 2"
                     /protein_id="AKQ20250.1"
                     /db_xref="GI:883741107"
                     /translation="MNPLAQPVIYSTIFAGTLITALSSHWFFTWVGLEMNMLAFIPVL
                     TKKMNPRSTEAAIKYFLTQATASMILLMAILFNNMLSGQWTMTNTTNQYSSLMIMMAM
                     AMKLGMAPFHFWVPEVTQGTPLTSGLLLLTWQKLAPISIMYQISPSLNVSLLLTLSIL
                     SIMAGSWGGLNQTQLRKILAYSSITHMGWMMAVLPYNPNMTILNLTIYIILTTTAFLL
                     LNLNSSTTTLLLSRTWNKLTWLTPLIPSTLLSLGGLPPLTGFLPKWAIIEEFTKNNSL
                     IIPTIMATITLLNLYFYLRLIYSTSITLLPMSNNVKMKWQFEHTKPTPFLPTLIALTT
                     LLLPISPFMLMIL"
     tRNA            5515..5582
                     /product="tRNA-Trp"
     tRNA            complement(5590..5658)
                     /product="tRNA-Ala"
     tRNA            complement(5660..5732)
                     /product="tRNA-Asn"
     tRNA            complement(5764..5829)
                     /product="tRNA-Cys"
     tRNA            complement(5829..5894)
                     /product="tRNA-Tyr"
     gene            5907..7448
                     /gene="COX1"
     CDS             5907..7448
                     /gene="COX1"
                     /codon_start=1
                     /transl_table=2
                     /product="cytochrome c oxidase subunit I"
                     /protein_id="AKQ20251.1"
                     /db_xref="GI:883741108"
                     /translation="MFADRWLFSTNHKDIGTLYLLFGAWAGVLGTALSLLIRAELGQP
                     GNLLGNDHIYNVIVTAHAFVMIFFMVMPIMIGGFGNWLVPLMIGAPDMAFPRMNNMSF
                     WLLPPSLLLLLASAMVEAGAGTGWTVYPPLAGNYSHPGASVDLTIFSLHLAGVSSILG
                     AINFITTIINMKPPAMTQYQTPLFVWSVLITAVLLLLSLPVLAAGITMLLTDRNLNTT
                     FFDPAGGGDPILYQHLFWFFGHPEVYILILPGFGMISHIVTYYSGKKEPFGYMGMVWA
                     MMSIGFLGFIVWAHHMFTVGMDVDTRAYFTSATMIIAIPTGVKVFSWLATLHGSNMKW
                     SAAVLWALGFIFLFTVGGLTGIVLANSSLDIVLHDTYYVVAHFHYVLSMGAVFAIMGG
                     FIHWFPLFSGYTLDQTYAKIHFTIMFIGVNLTFFPQHFLGLSGMPRRYSDYPDAYTTW
                     NILSSVGSFISLTAVMLMIFMIWEAFASKRKVLMVEEPSMNLEWLYGCPPPYHTFEEP
                     VYMKS"
     tRNA            complement(7448..7519)
                     /product="tRNA-Ser"
                     /note="codons recognized: UCN"
     tRNA            7521..7588
                     /product="tRNA-Asp"
     gene            7589..8272
                     /gene="COX2"
     CDS             7589..8272
                     /gene="COX2"
                     /codon_start=1
                     /transl_table=2
                     /product="cytochrome c oxidase subunit II"
                     /protein_id="AKQ20252.1"
                     /db_xref="GI:883741109"
                     /translation="MAHAAQVGLQDATSPIMEELITFHDHALMIIFLICFLVLYALFL
                     TLTTKLTNTNISDAQEMETVWTILPAIILVLIALPSLRILYMTDEVNDPSLTIKSIGH
                     QWYWTYEYTDYGGLIFNSYMLPPLFLEPGDLRLLDVDNRVVLPIEAPIRMMITSQDVL
                     HSWAVPTLGLKTDAIPGRLNQTTFTATRPGVYYGQCSEICGANHSFMPIVLELIPLKI
                     FEMGPVFTL"
     tRNA            8298..8367
                     /product="tRNA-Lys"
     gene            8369..8575
                     /gene="ATP8"
     CDS             8369..8575
                     /gene="ATP8"
                     /codon_start=1
                     /transl_table=2
                     /product="ATP synthase F0 subunit 8"
                     /protein_id="AKQ20253.1"
                     /db_xref="GI:883741110"
                     /translation="MPQLNTTVWPTMITPMLLTLFLITQLKMLNTNYHLPPSPKPMKM
                     KNYNKPWEPKWTKICSLHSLPPQS"
     gene            8530..9210
                     /gene="ATP6"
     CDS             8530..9210
                     /gene="ATP6"
                     /codon_start=1
                     /transl_table=2
                     /product="ATP synthase F0 subunit 6"
                     /protein_id="AKQ20254.1"
                     /db_xref="GI:883741111"
                     /translation="MNENLFASFIAPTILGLPAAVLIILFPPLLIPTSKYLINNRLIT
                     TQQWLIKLTSKQMMTMHNTKGRTWSLMLVSLIIFIATTNLLGLLPHSFTPTTQLSMNL
                     AMAIPLWAGAVIMGFRSKIKNALAHFLPQGTPTPLIPMLVIIETISLLIQPMALAVRL
                     TANITAGHLLMHLIGSATLAMSTINLPSTLIIFTILILLTILEIAVALIQAYVFTLLV
                     SLYLHDNT"
     gene            9210..9993
                     /gene="COX3"
     CDS             9210..9993
                     /gene="COX3"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:9993,aa:TERM)
                     /transl_table=2
                     /product="cytochrome c oxidase subunit III"
                     /protein_id="AKQ20255.1"
                     /db_xref="GI:883741112"
                     /translation="MTHQSHAYHMVKPSPWPLTGALSALLMTSGLAMWFHFHSMTLLM
                     LGLLTNTLTMYQWWRDVTRESTYQGHHTPPVQKGLRYGMILFITSEVFFFAGFFWAFY
                     HSSLAPTPQLGGHWPPTGITPLNPLEVPLLNTSVLLASGVSITWAHHSLMENNRNQMI
                     QALLITILLGLYFTLLQASEYFESPFTISDGIYGSTFFVATGFHGLHVIIGSTFLTIC
                     FIRQLMFHFTSKHHFGFEAAAWYWHFVDVVWLFLYVSIYWWGS"
     tRNA            9994..10061
                     /product="tRNA-Gly"
     gene            10062..10407
                     /gene="ND3"
     CDS             10062..10407
                     /gene="ND3"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:10407,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 3"
                     /protein_id="AKQ20256.1"
                     /db_xref="GI:883741113"
                     /translation="MNFALILMINTLLALLLMIITFWLPQLNGYMEKSTPYECGFDPM
                     SPARVPFSMKFFLVAITFLLFDLEIALLLPLPWALQTTNLPLMVMSSLLLIIILALSL
                     AYEWLQKGLDWTE"
     tRNA            10408..10472
                     /product="tRNA-Arg"
     gene            10473..10769
                     /gene="ND4L"
     CDS             10473..10769
                     /gene="ND4L"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 4L"
                     /protein_id="AKQ20257.1"
                     /db_xref="GI:883741114"
                     /translation="MPLIYMNIMLAFTISLLGMLVYRSHLMSSLLCLEGMMLSLFIMA
                     TLMTLNTHSLLANIVPIAMLVFAACEAAVGLALLVSISNTYGLDYVHNLNLLQC"
     gene            10763..12140
                     /gene="ND4"
     CDS             10763..12140
                     /gene="ND4"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:12140,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 4"
                     /protein_id="AKQ20258.1"
                     /db_xref="GI:883741115"
                     /translation="MLKLIVPTIMLLPLTWLSKKHMIWINTTTHSLIISIIPLLFFNQ
                     INNNLFSCSPTFSSDPLTTPLLMLTTWLLPLTIMASQRHLSSEPLSRKKLYLSMLISL
                     QISLIMTFTATELIMFYIFFETTLIPTLAIITRWGNQPERLNAGTYFLFYTLVGSLPL
                     LIALIYTHNTLGSLNILLLTLTAQELSNSWANNLMWLAYTMAFMVKMPLYGLHLWLPK
                     AHVEAPIAGSMVLAAVLLKLGGYGMMRLTLILNPLTKHMAYPFLVLSLWGMIMTSSIC
                     LRQTDLKSLIAYSSISHMALVVTAILIQTPWSFTGAVILMIAHGLTSSLLFCLANSNY
                     ERTHSRIMILSQGLQTLLPLMAFWWLLASLANLALPPTINLLGELSVLVTTFSWSNIT
                     LLLTGLNMLVTALYSLYMFTTTQWGSLTHHINNMKPSFTRENTLMFMHLSPILLLSLN
                     PDIITGFSS"
     tRNA            12141..12209
                     /product="tRNA-His"
     tRNA            12210..12268
                     /product="tRNA-Ser"
                     /note="codons recognized: AGY"
     tRNA            12269..12339
                     /product="tRNA-Leu"
                     /note="codons recognized: CUN"
     gene            12340..14151
                     /gene="ND5"
     CDS             12340..14151
                     /gene="ND5"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 5"
                     /protein_id="AKQ20259.1"
                     /db_xref="GI:883741116"
                     /translation="MTMHTTMTTLTLTSLIPPILTTLVNPNKKNSYPHYVKSIVASTF
                     IISLFPTTMFMCLDQEVIISNWHWATTQTTQLSLSFKLDYFSMMFIPVALFVTWSIME
                     FSLWYMNSDPNINQFFKYLLIFLITMLILVTANNLFQLFIGWEGVGIMSFLLISWWYA
                     RADANTAAIQAILYNRIGDIGFILALAWFILHSNSWDPQQMALLNANPSLTPLLGLLL
                     AAAGKSAQLGLHPWLPSAMEGPTPVSALLHSSTMVVAGIFLLIRFHPLAENSPLIQTL
                     TLCLGAITTLFAAVCALTQNDIKKIVAFSTSSQLGLMMVTIGINQPHLAFLHICTHAF
                     FKAMLFMCSGSIIHNLNNEQDIRKMGGLLKTMPLTSTSLTIGSLALAGMPFLTGFYSK
                     DHIIETANMSYTNAWALSITLIATSLTSAYSTRMILLTLTGQPRFPTLTNINENNPTL
                     LNPIKRLAAGSLFAGFLITNNISPASPFQTTIPLYLKLTALAVTFLGLLTALDLNYLT
                     NKLKMKSPLCTFYFSNMLGFYPSITHRTIPYLGLLTSQNLPLLLLDLTWLEKLLPKTI
                     SQHQISTSIITSTQKGMIKLYFLSFFFPLILTLLLIT"
     gene            complement(14152..14676)
                     /gene="ND6"
     CDS             complement(14152..14676)
                     /gene="ND6"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 6"
                     /protein_id="AKQ20260.1"
                     /db_xref="GI:883741117"
                     /translation="MMYALFLLSVGLVMGFVGFSSKPSPIYGGLVLIVSGVVGCVIIL
                     NFGGGYMGLMVFLIYLGGMMVVFGYTTAMAIEEYPEAWGSGVEVLVSVLVGLAMEVGL
                     VLWVKEYDGVVVVVNFNSVGSWMIYEGEGSGLIREDPIGAGALYDYGRWLVVVTGWTL
                     FVGVYIVIEIARGN"
     tRNA            complement(14677..14745)
                     /product="tRNA-Glu"
     gene            14750..15890
                     /gene="CYTB"
     CDS             14750..15890
                     /gene="CYTB"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:15890,aa:TERM)
                     /transl_table=2
                     /product="cytochrome b"
                     /protein_id="AKQ20261.1"
                     /db_xref="GI:883741118"
                     /translation="MTPMRKINPLMKLINHSFIDLPTPSNISAWWNFGSLLGACLILQ
                     ITTGLFLAMHYSPDASTAFSSIAHITRDVNYGWIIRYLHANGASMFFICLFLHIGRGL
                     YYGSFLYSETWNIGIILLLATMATAFMGYVLPWGQMSFWGATVITNLLSAIPYIGTDL
                     VQWIWGGYSVDSPTLTRFFTFHFILPFIIAALAALHLLFLHETGSNNPLGITSHSDKI
                     TFHPYYTIKDALGLLLFLLSLMTLTLFSPDLLGDPDNYTLANPLNTPPHIKPEWYFLF
                     AYTILRSVPNKLGGVLALLLSILILAMIPILHMPKQQSMMFRPLSQSLYWLLAADLLI
                     LTWIGGQPVSYPFTIIGQVASVLYFTTILILMPTISLIENKMLKWA"
     tRNA            15891..15956
                     /product="tRNA-Thr"
     tRNA            complement(15958..16026)
                     /product="tRNA-Pro"
ORIGIN      
        1 gatcacaggt ctatcaccct attaaccact cacgggagct ctccatgcat ttggtatctt
       61 tcgtttgggg ggtatgcacg cgatagcatt gcgagacgct ggagccggag caccctatgt
      121 cgcagtatct gtctttgatt cctgcctcat cctattattt atcgcaccta cgttcaatat
      181 tacaggcgaa catacttact aaagtgtgtt aattaattaa tgcttgtagg acataataat
      241 aacaattgaa tgtctgcaca gccgctttcc acacagacat cataacaaaa aatttccacc
      301 aaaccccccc cctccccccg cttctggcca cagcacttaa acacatctct gccaaacccc
      361 aaaaacaaag aaccctaaca ccagcctaac cagatttcaa attttatctt ttggcggtat
      421 gcacttttaa cagtcacccc ccaactaaca cattattttc ccctcccact cccatactac
      481 taatctcatc aatacaaccc ccgcccatcc tacccagcac acacacaccg ctgctaaccc
      541 cataccccga accaaccaaa ccccaaagac accccccaca gtttatgtag cttacctcct
      601 caaagcaata cactgaaaat gtttagacgg gctcacatca ccccataaac aaataggttt
      661 ggtcctagcc tttctattag ctcttagtaa gattacacat gcaagcatcc ccgttccagt
      721 gagttcaccc tctaaatcac cacgatcaaa aggracaagc atcaagcacg cagcaatgca
      781 gctcaaaacg cttagcctag ccacaccccc acgggaaaca gcagtgatta acctttagca
      841 ataaacgaaa gtttaactaa gctatactaa ccccagggtt ggtcaatttc gtgccagcca
      901 ccgcggtcac acgattaacc caagtcaata gaagccggcg taaagagtgt tttagatcac
      961 cccctcccca ataaagctaa aactcacctg agttgtaaaa aactccagtt gacacaaaat
     1021 agactacgaa agtggcttta acatatctga acacacaata gctaagaccc aaactgggat
     1081 tagatacccc actatgctta gccctaaacc tcaacagtta aatcaacaaa actgctcgcc
     1141 agaacactac gagccacagc ttaaaactca aaggacctgg cggtgcttca tatccctcta
     1201 gaggagcctg ttctgtaatc gataaacccc gatcaacctc accacctctt gctcagccta
     1261 tataccgcca tcttcagcaa accctgatga aggctacaaa gtaagcgcaa gtacccacgt
     1321 aaagacgtta ggtcaaggtg tagcccatga ggtggcaaga aatgggctac attttctacc
     1381 ccagaaaact acgatagccc ttatgaaact taagggtcga aggtggattt agcagtaaac
     1441 tgagagtaga gtgcttagtt gaacagggcc ctgaagcgcg tacacaccgc ccgtcaccct
     1501 cctcaagtat acttcaaagg acatttaact aaaaccccta cgcatttata tagaggagac
     1561 aagtcgtaac atggtaagtg tactggaaag tgcacttgga cgaaccagag tgtagcttaa
     1621 cacaaagcac ccaacttaca cttaggagat ttcaacttaa cttgaccgct ctgagctaaa
     1681 cctagcccca aacccactcc accttactac cagacaacct tagccaaacc atttacccaa
     1741 ataaagtata ggcgatagaa attgaaacct ggcgcaatag atatagtacc gcaagggaaa
     1801 gatgaaaaat tataaccaag cataatatag caaggactaa cccctatacc ttctgcataa
     1861 tgaattaact agaaataact ttgcaaggag agccaaagct aagacccccg aaaccagacg
     1921 agctacctaa gaacagctaa aagagcacac ccgtctatgt agcaaaatag tgggaagatt
     1981 tataggtaga ggcgacaaac ctaccgagcc tggtgatagc tggttgtcca agatagaatc
     2041 ttagttcaac tttaaatttg cccacagaac cctctaaatc cccttgtaaa tttaactgtt
     2101 agtccaaaga ggaacagctc tttggacact aggaaaaaac cttgtagaga gagtaaaaaa
     2161 tttaacaccc atagtaggcc taaaagcagc caccaattaa gaaagcgttc aagctcaaca
     2221 cccactacct aaaaaatccc aaacatataa ctgaactcct cacacccaat tggaccaatc
     2281 tatcacccta tagaagaact aatgttagta taagtaacat gaaaacattc tcctccgcat
     2341 aagcctgcgt cagattaaga cactgaactg acaattaaca gcccaatatc tacaatcaac
     2401 caacaagtca ttattaccct cactgtcaac ccaacacagg catgcccata aggaaaggtt
     2461 aaaaaaagta aaaggaactc ggcaaatctt accccgcctg tttaccaaaa acatcacctc
     2521 tagcatcacc agtattagag gcaccgcctg cccagtgaca catgtttaac ggccgcggta
     2581 ccctaaccgt gcaaaggtag cataatcact tgttccttaa atagggacct gtatgaatgg
     2641 ctccacgagg gttcagctgt ctcttacttt taaccagtga aattgacctg cccgtgaaga
     2701 ggcgggcatg acacagcaag acgagaagac cctatggagc tttaatttat taatgcaaac
     2761 agtacctaac aaacccacag gtcctaaact accaaacctg cattaaaaat ttcggttggg
     2821 gcgacctcgg agcagaaccc aacctccgag cagtacatgc taagacttca ccagtcaaag
     2881 cgaactacta tactcaattg atccaataac ttgaccaacg gaacaagtta ccctagggat
     2941 aacagcgcaa tcctattcta gagtccatat caacaatagg gtttacgacc tcgatgttgg
     3001 atcaggacat cccgatggtg cagccgctat taaaggttcg tttgttcaac gattaaagtc
     3061 ctacgtgatc tgagttcaga ccggagtaat ccaggtcggt ttctatctac ttcaaattcc
     3121 tccctgtacg aaaggacaag agaaataagg cctacttcac aaagcgcctt cccccgtaaa
     3181 tgatatcatc tcaacttagt attataccca cacccaccca agaacagggt ttgttaagat
     3241 ggcagagccc ggtaatcgca taaaacttaa aactttacag tcagaggttc aattcctctt
     3301 cttaacaaca tacccatggc caacctccta ctcctcattg tacccattct aatcgcaatg
     3361 gcattcctaa tgcttaccga acgaaaaatt ctaggctata tacaactacg caaaggcccc
     3421 aacgttgtag gcccctacgg gctactacaa cccttcgctg acgccataaa actcttcacc
     3481 aaagagcccc taaaacccgc cacatctacc atcaccctct acatcaccgc cccgacctta
     3541 gctctcacca tcgctcttct actatgaacc cccctcccca tacccaaccc cctggtcaac
     3601 ctcaacctag gcctcctatt tattctagcc acctctagcc tagccgttta ctcaatcctc
     3661 tgatcagggt gagcatcaaa ctcaaactac gccctgatcg gcgcactgcg agcagtagcc
     3721 caaacaatct catatgaagt caccctagcc atcattctac tatcaacatt actaataagt
     3781 ggctccttta acctctccac ccttatcaca acacaagaac acctctgatt actcctgcca
     3841 tcatgacccc tggccataat atgatttatc tccacactag cagagaccaa ccgaaccccc
     3901 ttcgaccttg ccgaagggga gtccgaacta gtctcaggct tcaacatcga atacgccgca
     3961 ggccccttcg ccctattctt catagccgaa tacacaaaca ttattataat aaacaccctc
     4021 accactacaa tcttcctagg aacaacatat gacgcactct cccctgaact ctacacaaca
     4081 tattttgtca ccaagaccct acttctaacc tccctgttct tatgaattcg aacagcatac
     4141 ccccgattcc gctacgacca actcatacac ctcctatgaa aaaacttcct accactcacc
     4201 ctagcattac ttatatgata tgtctccata cccattacaa tctccagcat tccccctcaa
     4261 acctaagaaa tatgtctgat aaaagagtta ctttgataga gtaaataata ggagcttaaa
     4321 cccccttatt tctaggacta tgagaatcga acccatccct gagaatccaa aattctccgt
     4381 gccacctatc acaccccatc ctaaagtaag gtcagctaaa taagctatcg ggcccatacc
     4441 ccgaaaatgt tggttatacc cttcccgtac taattaatcc cctggcccaa cccgtcatct
     4501 actctaccat ctttgcaggc acactcatca cagcgctaag ctcgcactga ttttttacct
     4561 gagtaggcct agaaataaac atgctagctt ttattccagt tctaaccaaa aaaataaacc
     4621 ctcgttccac agaagctgcc atcaagtatt tcctcacgca agcaaccgca tccataatcc
     4681 ttctaatagc tatcctcttc aacaatatac tctccggaca atgaaccata accaatacta
     4741 ccaatcaata ctcatcatta ataatcataa tggctatagc aataaaacta ggaatagccc
     4801 cctttcactt ctgagtccca gaggttaccc aaggcacccc tctgacatcc ggcctgcttc
     4861 ttctcacatg acaaaaacta gcccccatct caatcatata ccaaatctct ccctcactaa
     4921 acgtaagcct tctcctcact ctctcaatct tatccatcat agcaggcagt tgaggtggat
     4981 taaaccaaac ccagctacgc aaaatcttag catactcctc aattacccac ataggatgaa
     5041 taatagcagt tctaccgtac aaccctaaca taaccattct taatttaact atttatatta
     5101 tcctaactac taccgcattc ctactactca acttaaactc cagcaccacg accctactac
     5161 tatctcgcac ctgaaacaag ctaacatgac taacaccctt aattccatcc accctcctct
     5221 ccctaggagg cctgccccca ctaaccggct ttttgcccaa atgggccatt atcgaagaat
     5281 tcacaaaaaa caatagcctc atcatcccca ccatcatagc caccatcacc ctccttaacc
     5341 tctacttcta cctacgccta atctactcca cctcaatcac actactcccc atatctaaca
     5401 acgtaaaaat aaaatgacag tttgaacata caaaacccac cccattcctc cccacactca
     5461 tcgcccttac cacgctactc ctacctatct ccccttttat actaataatc ttatagaaat
     5521 ttaggttaaa tacagaccaa gagccttcaa agccctcagt aagttgcaat acttaatttc
     5581 tgtaacagct aaggactgca aaaccccact ctgcatcaac tgaacgcaaa tcagccactt
     5641 taattaagct aagcccttac tagaccaatg ggacttaaac ccacaaacac ttagttaaca
     5701 gctaagcacc ctaatcaact ggcttcaatc tacttctccc gccgccggga aaaaaggcgg
     5761 gagaagcccc ggcaggtttg aagctgcttc ttcgaatttg caattcaata tgaaaatcac
     5821 ctcggagctg gtaaaaagag gcctaacccc tgtctttaga tttacagtcc aatgcttcac
     5881 tcagccattt tacctcaccc ccactgatgt tcgccgaccg ttgactattc tctacaaacc
     5941 acaaagacat tggaacacta tacctattat tcggcgcatg agctggagtc ctaggcacag
     6001 ctctaagcct ccttattcga gccgagctgg gccagccagg caaccttcta ggtaacgacc
     6061 acatctacaa cgttatcgtc acagcccatg catttgtaat aatcttcttc atagtaatac
     6121 ccatcataat cggaggcttt ggcaactgac tagttcccct aataatcggt gcccccgata
     6181 tggcgtttcc ccgcataaac aacataagct tctgactctt acctccctct ctcctactcc
     6241 tgctcgcatc tgctatagtg gaggccggag caggaacagg ttgaacagtc taccctccct
     6301 tagcagggaa ctactcccac cctggagcct ccgtagacct aaccatcttc tccttacacc
     6361 tagcaggtgt ctcctctatc ttaggggcca tcaatttcat cacaacaatt atcaatataa
     6421 aaccccctgc cataacccaa taccaaacgc ccctcttcgt ctgatccgtc ctaatcacag
     6481 cagtcctact tctcctatct ctcccagtcc tagctgctgg catcactata ctactaacag
     6541 accgcaacct caacaccacc ttcttcgacc ccgccggagg aggagacccc attctatacc
     6601 aacacctatt ctgatttttc ggtcaccctg aagtttatat tcttatccta ccaggcttcg
     6661 gaataatctc ccatattgta acttactact ccggaaaaaa agaaccattt ggatacatag
     6721 gtatggtctg agctatgata tcaattggct tcctagggtt tatcgtgtga gcacaccata
     6781 tatttacagt aggaatagac gtagacacac gagcatattt cacctccgct accataatca
     6841 tcgctatccc caccggcgtc aaagtattta gctgactcgc cacactccac ggaagcaata
     6901 tgaaatgatc tgctgcagtg ctctgagccc taggattcat ctttcttttc accgtaggtg
     6961 gcctgactgg cattgtacta gcaaactcat cactagacat cgtactacac gacacgtact
     7021 acgttgtagc tcacttccac tatgtcctat caataggagc tgtatttgcc atcataggag
     7081 gcttcattca ctgatttccc ctattctcag gctacaccct agaccaaacc tacgccaaaa
     7141 tccatttcac tatcatattc atcggcgtaa atctaacttt cttcccacaa cactttctcg
     7201 gcctatccgg aatgccccga cgttactcgg actaccccga tgcatacacc acatgaaaca
     7261 tcctatcatc tgtaggctca ttcatttctc taacagcagt aatattaata attttcatga
     7321 tttgagaagc cttcgcttcg aagcgaaaag tcctaatagt agaagaaccc tccataaacc
     7381 tggagtgact atatggatgc cccccaccct accacacatt cgaagaaccc gtatacataa
     7441 aatctagaca aaaaaggaag gaatcgaacc ccccaaagct ggtttcaagc caaccccatg
     7501 gcctccatga ctttttcaaa aaggtattag aaaaaccatt tcataacttt gtcaaagtta
     7561 aattataggc taaatcctat atatcttaat ggcacatgca gcgcaagtag gtctacaaga
     7621 cgctacttcc cctatcatag aagagcttat cacctttcat gatcacgccc tcataatcat
     7681 tttccttatc tgcttcctag tcctgtatgc ccttttccta acactcacaa caaaactaac
     7741 taatactaac atctcagacg ctcaggaaat agaaaccgtc tgaactatcc tgcccgccat
     7801 catcctagtc ctcatcgccc tcccatccct acgcatcctt tacataacag acgaggtcaa
     7861 cgatccctcc cttaccatca aatcaattgg ccaccaatgg tactgaacct acgagtacac
     7921 cgactacggc ggactaatct tcaactccta catacttccc ccattattcc tagaaccagg
     7981 cgacctgcga ctccttgacg ttgacaatcg agtagtactc ccgattgaag cccccattcg
     8041 tataataatt acatcacaag acgtcttgca ctcatgagct gtccccacat taggcttaaa
     8101 aacagatgca attcccggac gtctaaacca aaccactttc accgctacac gaccgggggt
     8161 atactacggt caatgctctg aaatctgtgg agcaaaccac agtttcatgc ccatcgtcct
     8221 agaattaatt cccctaaaaa tctttgaaat agggcccgta tttaccctat agcaccccct
     8281 ctaccccctc tagagcccac tgtaaagcta acttagcatt aaccttttaa gttaaagatt
     8341 aagagaacca acacctcttt acagtgaaat gccccaacta aatactaccg tatggcccac
     8401 cataattacc cccatactcc ttacactatt cctcatcacc caactaaaaa tattaaacac
     8461 aaactaccac ctacctccct caccaaagcc cataaaaata aaaaattata acaaaccctg
     8521 agaaccaaaa tgaacgaaaa tctgttcgct tcattcattg cccccacaat cctaggccta
     8581 cccgccgcag tactgatcat tctatttccc cctctattga tccccacctc caaatatctc
     8641 atcaacaacc gactaatcac cacccaacaa tgactaatca aactaacctc aaaacaaatg
     8701 ataaccatac acaacactaa aggacgaacc tgatctctta tactagtatc cttaatcatt
     8761 tttattgcca caactaacct cctcggactc ctgcctcact catttacacc aaccacccaa
     8821 ctatctataa acctagccat ggccatcccc ttatgagcgg gcgcagtgat tataggcttt
     8881 cgctctaaga ttaaaaatgc cctagcccac ttcttaccac aaggcacacc tacacccctt
     8941 atccccatac tagttattat cgaaaccatc agcctactca ttcaaccaat agccctggcc
     9001 gtacgcctaa ccgctaacat tactgcaggc cacctactca tgcacctaat tggaagcgcc
     9061 accctagcaa tatcaaccat taaccttccc tctacactta tcatcttcac aattctaatt
     9121 ctactgacta tcctagaaat cgctgtcgcc ttaatccaag cctacgtttt cacacttcta
     9181 gtaagcctct acctgcacga caacacataa tgacccacca atcacatgcc tatcatatag
     9241 taaaacccag cccatgaccc ctaacagggg ccctctcagc cctcctaatg acctccggcc
     9301 tagccatgtg atttcacttc cactccataa cgctcctcat actaggccta ctaaccaaca
     9361 cactaaccat ataccaatga tggcgcgatg taacacgaga aagcacatac caaggccacc
     9421 acacaccacc tgtccaaaaa ggccttcgat acgggataat cctatttatt acctcagaag
     9481 tttttttctt cgcaggattt ttctgagcct tttaccactc cagcctagcc cctacccccc
     9541 aattaggagg gcactggccc ccaacaggca tcaccccgct aaatccccta gaagtcccac
     9601 tcctaaacac atccgtatta ctcgcatcag gagtatcaat cacctgagct caccatagtc
     9661 taatagaaaa caaccgaaac caaataattc aagcactgct tattacaatt ttactgggtc
     9721 tctattttac cctcctacaa gcctcagagt acttcgagtc tcccttcacc atttccgacg
     9781 gcatctacgg ctcaacattt tttgtagcca caggcttcca cggacttcac gtcattattg
     9841 gctcaacttt cctcactatc tgcttcatcc gccaactaat atttcacttt acatccaaac
     9901 atcactttgg cttcgaagcc gccgcctgat actggcattt tgtagatgtg gtttgactat
     9961 ttctgtatgt ctccatctat tgatgagggt cttactcttt tagtataaat agtaccgtta
    10021 acttccaatt aactagtttt gacaacattc aaaaaagagt aataaacttc gccttaattt
    10081 taataatcaa caccctccta gccttactac taataattat tacattttga ctaccacaac
    10141 tcaacggcta catagaaaaa tccacccctt acgagtgcgg cttcgaccct atatcccccg
    10201 cccgcgtccc tttctccata aaattcttct tagtagctat taccttctta ttatttgatc
    10261 tagaaattgc cctcctttta cccctaccat gagccctaca aacaactaac ctgccactaa
    10321 tagttatgtc atccctctta ttaatcatca tcctagccct aagtctggcc tatgagtgac
    10381 tacaaaaagg attagactga accgaattgg tatatagttt aaacaaaacg aatgatttcg
    10441 actcattaaa ttatgataat catatttacc aaatgcccct catttacata aatattatac
    10501 tagcatttac catctcactt ctaggaatac tagtatatcg ctcacacctc atatcctccc
    10561 tactatgcct agaaggaata atactatcgc tgttcattat agctactctc ataaccctca
    10621 acacccactc cctcttagcc aatattgtgc ctattgccat actagtcttt gccgcctgcg
    10681 aagcagcggt gggcctagcc ctactagtct caatctccaa cacatatggc ctagactacg
    10741 tacataacct aaacctactc caatgctaaa actaatcgtc ccaacaatta tattactacc
    10801 actgacatga ctttccaaaa aacacataat ttgaatcaac acaaccaccc acagcctaat
    10861 tattagcatc atccctctac tattttttaa ccaaatcaac aacaacctat ttagctgttc
    10921 cccaaccttt tcctccgacc ccctaacaac ccccctccta atactaacta cctgactcct
    10981 acccctcaca atcatggcaa gccaacgcca cttatccagt gaaccactat cacgaaaaaa
    11041 actctacctc tctatactaa tctccctaca aatctcctta attataacat tcacagccac
    11101 agaactaatc atattttata tcttcttcga aaccacactt atccccacct tggctatcat
    11161 cacccgatga ggcaaccagc cagaacgcct gaacgcaggc acatacttcc tattctacac
    11221 cctagtaggc tcccttcccc tactcatcgc actaatttac actcacaaca ccctaggctc
    11281 actaaacatt ctactactca ctctcactgc ccaagaacta tcaaactcct gagccaacaa
    11341 cttaatatga ctagcttaca caatagcttt tatagtaaag atacctcttt acggactcca
    11401 cttatgactc cctaaagccc atgtcgaagc ccccatcgct gggtcaatag tacttgccgc
    11461 agtactctta aaactaggcg gctatggtat aatacgcctc acactcattc tcaaccccct
    11521 gacaaaacac atagcctacc ccttccttgt actatcccta tgaggcataa ttataacaag
    11581 ctccatctgc ctacgacaaa cagacctaaa atcgctcatt gcatactctt caatcagcca
    11641 catagccctc gtagtaacag ccattctcat ccaaaccccc tgaagcttca ccggcgcagt
    11701 cattctcata atcgcccacg ggcttacatc ctcattacta ttctgcctag caaactcaaa
    11761 ctacgaacgc actcacagtc gcatcataat cctctctcaa ggacttcaaa ctctactccc
    11821 actaatagct ttttgatgac ttctagcaag cctcgctaac ctcgccttac cccccactat
    11881 taacctactg ggagaactct ctgtgctagt aaccacgttc tcctgatcaa atatcactct
    11941 cctacttaca ggactcaaca tactagtcac agccctatac tccctctaca tatttaccac
    12001 aacacaatgg ggctcactca cccaccacat taacaacata aaaccctcat tcacacgaga
    12061 aaacaccctc atgttcatac acctatcccc cattctcctc ctatccctca accccgacat
    12121 cattaccggg ttttcctctt gtaaatatag tttaaccaaa acatcagatt gtgaatctga
    12181 caacagaggc ttacgacccc ttatttaccg agaaagctca caagaactgc taactcatgc
    12241 ccccatgtct aacaacatgg ctttctcaac ttttaaagga taacagctat ccattggtct
    12301 taggccccaa aaattttggt gcaactccaa ataaaagtaa taaccatgca cactactata
    12361 accaccctaa ccctgacttc cctaattccc cccatcctta ccaccctcgt taaccctaac
    12421 aaaaaaaact cataccccca ttatgtaaaa tccattgtcg catccacctt tattatcagt
    12481 ctcttcccca caacaatatt catgtgccta gaccaagaag ttattatctc gaactgacac
    12541 tgagccacaa cccaaacaac ccagctctcc ctaagcttca aactagacta cttctccata
    12601 atattcatcc ctgtagcatt gttcgttaca tggtccatca tagaattctc actgtgatat
    12661 ataaactcag acccaaacat taatcagttc ttcaaatatc tactcatctt cctaattacc
    12721 atactaatct tagttaccgc taacaaccta ttccaactgt tcatcggctg agagggcgta
    12781 ggaattatat ccttcttgct catcagttga tgatacgccc gagcagatgc caacacagca
    12841 gccattcaag caatcctata caaccgtatc ggcgatatcg gtttcatcct cgccttagca
    12901 tgatttatcc tacactccaa ctcatgagac ccacaacaaa tagcccttct aaacgctaat
    12961 ccaagcctca ccccactact aggcctcctc ctagcagcag caggcaaatc agcccaatta
    13021 ggtctccacc cctgactccc ctcagccata gaaggcccca ccccagtctc agccctactc
    13081 cactcaagca ctatagttgt agcaggaatc ttcttactca tccgcttcca ccccctagca
    13141 gaaaatagcc cactaatcca aactctaaca ctatgcttag gcgctatcac tactctgttc
    13201 gcagcagtct gcgcccttac acaaaatgac atcaaaaaaa tcgtagcctt ctccacttca
    13261 agtcaactag gactcataat agttacaatc ggcatcaacc aaccacacct agcattcctg
    13321 cacatctgta cccacgcctt cttcaaagcc atactattta tgtgctccgg gtccatcatc
    13381 cacaacctta acaatgaaca agatattcga aaaataggag gactactcaa aaccatacct
    13441 ctcacttcaa cctccctcac cattggcagc ctagcattag caggaatacc tttcctcaca
    13501 ggtttctact ccaaagacca catcatcgaa accgcaaaca tatcatacac aaacgcctga
    13561 gccctatcta ttactctcat cgctacctcc ctgacaagcg cctatagcac tcgaataatt
    13621 cttctcaccc taacaggtca acctcgcttc cccaccctta ctaacattaa cgaaaataac
    13681 cccaccctac taaaccccat taaacgcctg gcagccggaa gcctattcgc aggatttctc
    13741 attactaaca acatttcccc cgcatccccc ttccaaacaa caatccccct ctacctaaaa
    13801 ctcacagccc tcgctgtcac tttcctagga cttctaacag ccctagacct caactaccta
    13861 accaacaaac ttaaaataaa atccccacta tgcacatttt atttctccaa catactcgga
    13921 ttctacccta gcatcacaca ccgcacaatc ccctatctag gccttcttac gagccaaaac
    13981 ctgcccctac tcctcctaga cctaacctga ctagaaaagc tattacctaa aacaatttca
    14041 cagcaccaaa tctccacctc catcatcacc tcaacccaaa aaggcataat taaactttac
    14101 ttcctctctt tcttcttccc actcatccta accctactcc taatcacata acctattccc
    14161 ccgagcaatc tcaattacaa tatatacacc aacaaacaat gttcaaccag taactactac
    14221 taatcaacgc ccataatcat acaaagcccc cgcaccaata ggatcctccc gaatcaaccc
    14281 tgacccctct ccttcataaa ttattcagct tcctacacta ttaaagttta ccacaaccac
    14341 caccccatca tactctttca cccacagcac caatcctacc tccatcgcta accccactaa
    14401 aacactcacc aagacctcaa cccctgaccc ccatgcctca ggatactcct caatagccat
    14461 cgctgtagta tatccaaaga caaccatcat tccccctaaa taaattaaaa aaactattaa
    14521 acccatataa cctcccccaa aattcagaat aataacacac ccgaccacac cgctaacaat
    14581 caatactaaa cccccataaa taggagaagg cttagaagaa aaccccacaa accccattac
    14641 taaacccaca ctcaacagaa acaaagcata catcattatt ctcgcacgga ctacaaccac
    14701 gaccaatgat atgaaaaacc atcgttgtat ttcaactaca agaacaccaa tgaccccaat
    14761 acgcaaaatt aaccccctaa taaaattaat taaccactca ttcatcgacc tccccacccc
    14821 atccaacatc tccgcatgat gaaacttcgg ctcactcctt ggcgcctgcc tgatcctcca
    14881 aatcaccaca ggactattcc tagccatgca ctactcacca gacgcctcaa ccgccttttc
    14941 atcaatcgcc cacatcactc gagacgtaaa ttatggctga atcatccgct accttcacgc
    15001 caatggcgcc tcaatattct ttatctgcct cttcctacac atcgggcgag gcctatatta
    15061 cggatcattt ctctactcag aaacctgaaa catcggcatt atcctcctgc ttgcaactat
    15121 agcaacagcc ttcataggct atgtcctccc gtgaggccaa atatcattct gaggggccac
    15181 agtaattaca aacttactat ccgccatccc atacattggg acagacctag ttcaatgaat
    15241 ctgaggaggc tactcagtag acagtcccac cctcacacga ttctttacct ttcacttcat
    15301 cttgcccttc attattgcag ccctagcagc actccacctc ctattcttgc acgaaacggg
    15361 atcaaacaac cccctaggaa tcacctccca ttccgataaa atcaccttcc acccttacta
    15421 cacaatcaaa gacgccctcg gcttacttct cttccttctc tccttaatga cattaacact
    15481 attctcacca gacctcctag gcgacccaga caattatacc ctagccaacc ccttaaacac
    15541 ccctccccac atcaagcccg aatgatattt cctattcgcc tacacaattc tccgatccgt
    15601 ccctaacaaa ctaggaggcg tccttgccct attactatcc atcctcatcc tagcaataat
    15661 ccccatcctc catataccca aacaacaaag cataatattt cgcccactaa gccaatcact
    15721 ttattgactc ctagccgcag acctcctcat tctaacctga atcggaggac aaccagtaag
    15781 ctaccctttt accatcattg gacaagtagc atccgtacta tacttcacaa caatcctaat
    15841 cctaatacca actatctccc taattgaaaa caaaatactc aaatgggcct gtccttgtag
    15901 tataaactaa tacaccagtc ttgtaagccg gagatgaaaa cctttttcca aggacaaatc
    15961 agagaaaaag tctttaactc caccattagc acccaaagct aagattctaa tttaaactat
    16021 tctctgttct ttcatgggga agcagatttg ggtaccaccc aagtattgac tcacccatca
    16081 acaaccgcta tgtatttcgt acattactgc cagccaccat gaatattgca cggtaccata
    16141 aatacttgac cacctgtagt acataaaaac ccaatccaca tcaaaacccc ctccccatgc
    16201 ttacaagcaa gtacagcaat caaccctcaa ctatcacaca tcaactgcaa ctccaaagcc
    16261 acccctcacc cactaggata ccaacaaacc tacccaccct taacagttca tagtacataa
    16321 agccatttac cgtacatagc acattacagt caaatccctt ctcgccccca tggatgaccc
    16381 ccctcagata ggggtccctt gaccaccatc ctccgtgaaa tcaatatccc gcacaagagt
    16441 gctactctcc tcgctccggg cccataacac ttgggggtag ctaaagtgaa ctgtatccga
    16501 catctggttc ctacttcagg gtcataaagc ctaaatagcc cacacgttcc ccttaaataa
    16561 gacatcacga tg
//

