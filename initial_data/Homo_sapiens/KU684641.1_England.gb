LOCUS       KU684641               16571 bp    DNA     circular PRI 24-FEB-2016
DEFINITION  Homo sapiens haplogroup M1a3b mitochondrion, complete genome.
ACCESSION   KU684641
VERSION     KU684641.1  GI:998491700
KEYWORDS    .
SOURCE      mitochondrion Homo sapiens (human)
  ORGANISM  Homo sapiens
            Eukaryota; Metazoa; Chordata; Craniata; Vertebrata; Euteleostomi;
            Mammalia; Eutheria; Euarchontoglires; Primates; Haplorrhini;
            Catarrhini; Hominidae; Homo.
REFERENCE   1  (bases 1 to 16571)
  AUTHORS   Greenspan,B.
  TITLE     Direct Submission
  JOURNAL   Submitted (08-FEB-2016) Family Tree DNA - Genealogy by Genetics,
            Ltd., 1445 North Loop West, Suite 820, Houston, TX 77008, USA
FEATURES             Location/Qualifiers
     source          1..16571
                     /organism="Homo sapiens"
                     /organelle="mitochondrion"
                     /mol_type="genomic DNA"
                     /isolate="FTDNA 277412"
                     /db_xref="taxon:9606"
                     /haplogroup="M1a3b"
                     /note="ethnicity:English"
     D-loop          complement(join(16026..16571,1..579))
     tRNA            580..650
                     /product="tRNA-Phe"
     rRNA            651..1604
                     /product="12S ribosomal RNA"
     tRNA            1605..1673
                     /product="tRNA-Val"
     rRNA            1674..3231
                     /product="16S ribosomal RNA"
     tRNA            3232..3306
                     /product="tRNA-Leu"
     gene            3309..4264
                     /gene="ND1"
     CDS             3309..4264
                     /gene="ND1"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:4263..4264,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 1"
                     /protein_id="AMK49558.1"
                     /db_xref="GI:998491701"
                     /translation="MPMANLLLLIVPILIAMAFLMLTERKILGYMQLRKGPNVVGPYG
                     LLQPFADAMKLFTKEPLKPATSTITLYITAPTLALTIALLLWTPLPMPNPLVNLNLGL
                     LFILATSSLAVYSILWSGWASNSNYALIGALRAVAQTISYEVTLAIILLSTLLMSGSF
                     NLSTLITTQEHLWLLLPSWPLAMMWFISTLAETNRTPFDLAEGESELVSGFNIEYAAG
                     PFALFFMAEYTNIIMMNTLTTTIFLGTTYDALSPELYTTYFVTKTLLLTSLFLWIRTA
                     YPRFRYDQLMHLLWKNFLPLTLALLMWYVSMPITISSIPPQT"
     tRNA            4265..4333
                     /product="tRNA-Ile"
     tRNA            complement(4331..4402)
                     /product="tRNA-Gln"
     tRNA            4404..4471
                     /product="tRNA-Met"
     gene            4472..5513
                     /gene="ND2"
     CDS             4472..5513
                     /gene="ND2"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:5513,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 2"
                     /protein_id="AMK49559.1"
                     /db_xref="GI:998491702"
                     /translation="MNPLAQPVIYSTIFAGTLITALSSHWFFTWVGLEMNMLAFIPVL
                     TKKMNPRSTEAAIKYFLTQATASMILLMAILFNNMLSGQWTMTNTTNQYSSLMIMMAM
                     AMKLGMAPFHFWVPEVTQGTPLTSGLLLLTWQKLAPISIMYQISPSLNVSLLLTLSIL
                     SIMAGSWGGLNQTQLRKILAYSSITHMGWMMAVLPYNPNMTILNLTIYIILTTTAFLL
                     LNLNSSTTTLLLSRTWNKLTWLTPLIPSTLLSLGGLPPLTGFLPKWAIIEEFTKNNSL
                     IIPTIMATITLLNLYFYLRLIYSTSITLLPMSNNVKMKWQFEHTKPTPFLPTLIALTT
                     LLLPISPFMLMIL"
     tRNA            5514..5578
                     /product="tRNA-Trp"
     tRNA            complement(5589..5657)
                     /product="tRNA-Ala"
     tRNA            complement(5659..5731)
                     /product="tRNA-Asn"
     tRNA            complement(5763..5828)
                     /product="tRNA-Cys"
     tRNA            complement(5828..5893)
                     /product="tRNA-Tyr"
     gene            5906..7447
                     /gene="COX1"
     CDS             5906..7447
                     /gene="COX1"
                     /codon_start=1
                     /transl_table=2
                     /product="cytochrome c oxidase subunit I"
                     /protein_id="AMK49560.1"
                     /db_xref="GI:998491703"
                     /translation="MFADRWLFSTNHKDIGTLYLLFGAWAGVLGTALSLLIRAELGQP
                     GNLLGNDHIYNVIVTAHAFVMIFFMVMPIMIGGFGNWLVPLMIGAPDMAFPRMNNMSF
                     WLLPPSLLLLLASAMVEAGAGTGWTVYPPLAGNYSHPGASVDLTIFSLHLAGVSSILG
                     AINFITTIINMKPPAMTQYQTPLFVWSVLITAVLLLLSLPVLAAGITMLLTDRNLNTT
                     FFDPAGGGDPILYQHLFWFFGHPEVYILILPGFGMISHIVTYYSGKKEPFGYMGMVWA
                     MMSIGFLGFIVWAHHMFTVGMDVDTRAYFTSATMIIAIPTGVKVFSWLATLHGSNMKW
                     SAAVLWALGFIFLFTVGGLTGIVLANSSLDIVLHDTYYVVAHFHYVLSMGAVFAIMGG
                     FIHWFPLFSGYTLDQTYAKIHFTIMFIGVNLTFFPQHFLGLSGMPRRYSDYPDAYTTW
                     NILSSVGSFISLTAVMLMIFMIWEAFASKRKVLMVEEPSMNLEWLYGCPPPYHTFEEP
                     VYMKS"
     tRNA            complement(7447..7518)
                     /product="tRNA-Ser"
     tRNA            7520..7587
                     /product="tRNA-Asp"
     gene            7588..8271
                     /gene="COX2"
     CDS             7588..8271
                     /gene="COX2"
                     /codon_start=1
                     /transl_table=2
                     /product="cytochrome c oxidase subunit II"
                     /protein_id="AMK49561.1"
                     /db_xref="GI:998491704"
                     /translation="MAHAAQVGLQDATSPIMEELITFHDHALMIIFLICFLVLYALFL
                     TLTTKLTNTNISDAQEMETVWTILPAIILVLIALPSLRILYMTDEVNDPSLTIKSIGH
                     QWYWTYEYTDYGGLIFNSYMLPPLFLEPGDLRLLDVDNRVVLPIEAPIRMMITSQDVL
                     HSWAVPTLGLKTDAIPGRLNQTTFTATRPGVYYGQCSEICGANHSFMPIVLELIPLKI
                     FEMGPVFTL"
     tRNA            8297..8366
                     /product="tRNA-Lys"
     gene            8368..8574
                     /gene="ATP8"
     CDS             8368..8574
                     /gene="ATP8"
                     /codon_start=1
                     /transl_table=2
                     /product="ATP synthase F0 subunit 8"
                     /protein_id="AMK49562.1"
                     /db_xref="GI:998491705"
                     /translation="MPQLNTTVWPTMITPMLLTLFLITQLKMLNTNYHLPPSPKPMKM
                     KNYNKPWEPKWTKICSLHSLPPQS"
     gene            8529..9209
                     /gene="ATP6"
     CDS             8529..9209
                     /gene="ATP6"
                     /codon_start=1
                     /transl_table=2
                     /product="ATP synthase F0 subunit 6"
                     /protein_id="AMK49563.1"
                     /db_xref="GI:998491706"
                     /translation="MNENLFASFIAPTILGLPAAVLIILFPPLLIPTSKYLINNRLIT
                     TQQWLIKLTSKQMMAMHNTKGRTWSLMLVSLIIFIATTNLLGLLPHSFTPTTQLSMNL
                     AMAIPLWAGAVIMGFRSKIKNALAHFLPQGTPTPLIPMLVIIETISLLIQPMALAVRL
                     TANITAGHLLMHLIGSATLAMSTINLPSTLIIFTILILLTILEIAVALIQAYVFTLLV
                     SLYLHDNT"
     gene            9209..9992
                     /gene="COX3"
     CDS             9209..9992
                     /gene="COX3"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:9992,aa:TERM)
                     /transl_table=2
                     /product="cytochrome c oxidase subunit III"
                     /protein_id="AMK49564.1"
                     /db_xref="GI:998491707"
                     /translation="MTHQSHAYHMVKPSPWPLTGALSALLMTSGLAMWFHFHSMTLLM
                     LGLLTNTLTMYQWWRDVTRESTYQGHHTPPVQKGLRYGMILFITSEVFFFAGFFWAFY
                     HSSLAPTPQLGGHWPPTGITPLNPLEVPLLNTSVLLASGVSITWAHHSLMENNRNQMI
                     QALLITILLGLYFTLLQASEYFESPFTISDGIYGSTFFVATGFHGLHVIIGSTFLTIC
                     FIRQLMFHFTSKHHFGFEAAAWYWHFVDVVWLFLYVSIYWWGS"
     tRNA            9993..10060
                     /product="tRNA-Gly"
     gene            10061..10406
                     /gene="ND3"
     CDS             10061..10406
                     /gene="ND3"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:10406,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 3"
                     /protein_id="AMK49565.1"
                     /db_xref="GI:998491708"
                     /translation="MNFALILMINTLLALLLMIITFWLPQLNGYMEKSTPYECGFDPM
                     SPARVPFSMKFFLVAITFLLFDLEIALLLPLPWALQTTNLPLMVMSSLLLIIILALSL
                     AYEWLQKGLDWAE"
     tRNA            10407..10471
                     /product="tRNA-Arg"
     gene            10472..10768
                     /gene="ND4L"
     CDS             10472..10768
                     /gene="ND4L"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 4L"
                     /protein_id="AMK49566.1"
                     /db_xref="GI:998491709"
                     /translation="MPLIYMNIMLAFTISLLGMLVYRSHLMSSLLCLEGMMLSLFIMA
                     TLMTLNTHSLLANIVPIAMLVFAACEAAVGLALLVSISNTYGLDYVHNLNLLQC"
     gene            10762..12139
                     /gene="ND4"
     CDS             10762..12139
                     /gene="ND4"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:12139,aa:TERM)
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 4"
                     /protein_id="AMK49567.1"
                     /db_xref="GI:998491710"
                     /translation="MLKLIVPTIMLLPLTWLSKKHMIWINTTTHSLIISIIPLLFFNQ
                     INNNLFSCSPTFSSDPLTTPLLMLTTWLLPLTIMASQRHLSSEPLSRKKLYLSMLISL
                     QISLIMTFTATELIMFYIFFETTLIPTLAIITRWGNQPERLNAGTYFLFYTLVGSLPL
                     LIALIYTHNTLGSLNILLLTLTAQELSNSWANNLMWLAYTMAFMVKMPLYGLHLWLPK
                     AHVEAPIAGSMVLAAVLLKLGGYGMMRLTLILNPLTKHMAYPFLVLSLWGMIMTSSIC
                     LRQTDLKSLIAYSSISHMALVVTAILIQTPWSFTGAVILMIAHGLTSSLLFCLANSNY
                     ERTHSRIMILSQGLQTLLPLMAFWWLLASLANLALPPTINLLGELSVLVTTFSWSNIT
                     LLLTGLNMLVTALYSLYMFTTTQWGSLTHHINNMKPSFTRENTLMFMHLSPILLLSLN
                     PDIITGFSS"
     tRNA            12140..12208
                     /product="tRNA-His"
     tRNA            12209..12267
                     /product="tRNA-Ser"
     tRNA            12268..12338
                     /product="tRNA-Leu"
     gene            12339..14150
                     /gene="ND5"
     CDS             12339..14150
                     /gene="ND5"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 5"
                     /protein_id="AMK49568.1"
                     /db_xref="GI:998491711"
                     /translation="MTMHTTMTTLTLTSLIPPILTTFVNPNKKNSYPHYVKSIVASTF
                     IISLFPTTMFMCLDQEVIISNWHWATTQTTQLSLSFKLDYFSMMFIPVALFVTWSIME
                     FSLWYMNSDPNINQFFKYLLIFLITMLILVTANNLFQLFIGWEGVGIMSFLLISWWYA
                     RADANTAAIQAILYNRIGDIGFILALAWFILHSNSWDPQQMALLTANPSLTPLLGLLL
                     AAAGKSAQLGLHPWLPSAMEGPTPVSALLHSSTMVVAGIFLLIRFHPLAENSPLIQTL
                     TLCLGAITTLFAAVCALTQNDIKKIVAFSTSSQLGLMMVTIGINQPHLAFLHICTHAF
                     FKAMLFMCSGSIIHNLNNEQDIRKMGGLLKTMPLTSTSLTIGSLALAGMPFLTGFYSK
                     DHIIETANMSYTNAWALSITLIATSLTSAYSTRMILLTLTGRPRFPTLTNINENNPTL
                     LNPIKRLAAGSLFAGFLITNNISPASPFQTTIPLYLKLTALAVTFLGLLTALDLNYLT
                     NKLKMKSPLCTFYFSNMLGFYPSITHRTIPYLGLLTSQNLPLLLLDLTWLEKLLPKTI
                     SQHQISTSIITSTQKGMIKLYFLSFLFPLILTLLLIT"
     gene            complement(14151..14675)
                     /gene="ND6"
     CDS             complement(14151..14675)
                     /gene="ND6"
                     /codon_start=1
                     /transl_table=2
                     /product="NADH dehydrogenase subunit 6"
                     /protein_id="AMK49569.1"
                     /db_xref="GI:998491712"
                     /translation="MMYALFLLSVGLVMGFVGFSSKPSPIYGGLVLIVSGVVGCVIIL
                     NFGGGYMGLMVFLIYLGGMMVVFGYTTAMAIEEYPEAWGSGVEVLVSVLVGLAMEVGL
                     VLWVKEYDGVVVVVNFNSVGSWMIYEGEGSGLIREDPIGAGALYDYGRWLVVVTGWTL
                     FVGVYIVIEIARGN"
     tRNA            complement(14676..14744)
                     /product="tRNA-Glu"
     gene            14749..15889
                     /gene="CYTB"
     CDS             14749..15889
                     /gene="CYTB"
                     /note="TAA stop codon is completed by the addition of 3' A
                     residues to the mRNA"
                     /codon_start=1
                     /transl_except=(pos:15889,aa:TERM)
                     /transl_table=2
                     /product="cytochrome b"
                     /protein_id="AMK49570.1"
                     /db_xref="GI:998491713"
                     /translation="MTPMRKINPLMKLINHSFIDLPTPSNISAWWNFGSLLGACLILQ
                     ITTGLFLAMHYSPDASTAFSSIAHITRDVNYGWIIRYLHANGASMFFICLFLHIGRGL
                     YYGSFLYSETWNIGIILLLATMATAFMGYVLPWGQMSFWGATVITNLLSAIPYIGTDL
                     VQWIWGGYSVDSPTLTRFFTFHFILPFIIAALAALHLLFLHETGSNNPLGITSHSDKI
                     TFHPYYTIKDALGLLLFLLSLMTLTLFSPDLLGDPDNYTPANPLNTPPHIKPEWYFLF
                     AYTILRSVPNKLGGVLALLLSILILAMIPILHMSKQQSMMFRPLSQSLYWLLAADLLI
                     LTWIGGQPVSYPFTIIGQVASVLYFTTILILMPTISLIENKMLKWA"
     tRNA            15890..15955
                     /product="tRNA-Thr"
     tRNA            complement(15957..16025)
                     /product="tRNA-Pro"
ORIGIN      
        1 gatcacaggt ctatcaccct attaaccact cacgggagct ctccatgcat ttggtatttt
       61 cgtctggggg gtgtgcacgc gatagcattg cgagacgctg gagccggagc accctatgtc
      121 gcagtatctg tctttgattc ctgcctcatc ctattattta tcgcacctac gttcaatatt
      181 acaggcgaac atatctacta aagtgtgtta attaattaat gcttgtagga cataataata
      241 acaattgaat gtctgcacag ccgctttcca cacagacatc ataacaaaaa atttccacca
      301 aacccccccc ctccccccgc ttctggccac agcacttaaa cacatctctg ccaaacccca
      361 aaaacaaaga accctaacac cagcctaacc agatttcaaa ttttatcttt tggcggtatg
      421 cacttttaac agtcaccccc caactaacac attattttcc cctcccactc ccatactact
      481 aatctcatca acacaacccc cgcccatcct acccagcaca cacacaccgc tgctaacccc
      541 ataccccgaa ccaaccaaac cccaaagaca ccccccacag tttatgtagc ttacctcctc
      601 aaagcaatac actgaaaatg tttagacggg ctcacatcac cccataaaca aataggtttg
      661 gtcctagcct ttctattagc tcttagtaag attacacatg caagcatccc cgttccagtg
      721 agttcaccct ctaaatcacc acgatcaaaa gggacaagca tcaagcacgc agcaatgcag
      781 ctcaaaacgc ttagcctagc cacaccccca cgggagacag cagtgattaa cctttagcaa
      841 taaacgaaag tttaactaag ctatactaac cccagggttg gtcaatttcg tgccagccac
      901 cgcggtcaca cgattaaccc aagtcaatag aagccggcgt aaagagtgtt ttagatcacc
      961 ccctccccaa taaagctaaa actcacctga gttgtaaaaa actccagttg acacaaaata
     1021 gactacgaaa gtggctttaa catatctgaa cacacaatag ctaagaccca aactgggatt
     1081 agatacccca ctatgcttag ccctaaacct caacagttaa atcaacaaaa ctgctcgcca
     1141 gaacactacg agccacagct taaaactcaa aggacctggc ggtgcttcat atccctctag
     1201 aggagcctgt tctgtaatcg ataaaccccg atcaacctca ccacctcttg ctcagcctat
     1261 ataccgccat cttcagcaaa ccctgatgaa ggctacaaag taagcgcaag tacccacgta
     1321 aagacgttag gtcaaggtgt agcccatgag gtggcaagaa atgggctaca ttttctaccc
     1381 cagaaaacta cgatagccct tatgaaactt aagggtcgaa ggtggattta gcagtaaact
     1441 gagagtagag tgcttagttg aacagggccc tgaagcgcgt acacaccgcc cgtcaccctc
     1501 ctcaagtata cttcaaagga catttaacta aaacccctac gcatttatat agaggagaca
     1561 agtcgtaaca tggtaagtgt actggaaagt gcacttggac gaaccagagt gtagcttaac
     1621 acaaagcacc caacttacac ttaggagatt tcaacttaac ttgaccgctc tgagctaaac
     1681 ctagccccaa acccactcca ccttactacc agacaacctt agccaaacca tttacccaaa
     1741 taaagtatag gcgatagaaa ttgaaacctg gcgcaataga tatagtaccg caagggaaag
     1801 atgaaaaatt ataaccaagc ataatatagc aaggactaac ccctatacct tctgcataat
     1861 gaattaacta gaaataactt tgcaaggaga gccaaagcta agacccccga aaccagacga
     1921 gctacctaag aacagctaaa agagcacacc cgtctatgta gcaaaatagt gggaagattt
     1981 ataggtagag gcgacaaacc taccgagcct ggtgatagct ggttgtccaa gatagaatct
     2041 tagttcaact ttaaatttgc ccacagaacc ctctaaatcc ccttgtaaat ttaactgtta
     2101 gtccaaagag gaacagctct ttggacacta ggaaaaaacc ttgtagagag agtaaaaaat
     2161 ttaacaccca tagtaggcct aaaagcagcc accaattaag aaagcgttca agctcaacac
     2221 ccactaccta aaaaatccca aacatataac tgaactcctc acacccaatt ggaccaatct
     2281 atcaccctat agaagaacta atgttagtat aagtaacatg aaaacattct cctccgcata
     2341 agcctgcgtc agattaaaac actgaactga caattaacag cccaatatct acaatcaacc
     2401 aacaagtcat tattaccctc actgtcaacc caacacaggc atgctcataa ggaaaggtta
     2461 aaaaaagtaa aaggaactcg gcaaatctta ccccgcctgt ttaccaaaaa catcacctct
     2521 agcatcacca gtattagagg caccgcctgc ccagtgacac atgtttaacg gccgcggtac
     2581 cctaaccgtg caaaggtagc ataatcactt gttccttaaa tagggacctg tatgaatggc
     2641 tccacgaggg ttcagctgtc tcttactttt aaccagtgaa attgacctgc ccgtgaagag
     2701 gcgggcatga cacagcaaga cgagaagacc ctatggagct ttaatttatt aatgcaaaca
     2761 gtacctaaca aacccacagg tcctaaacta ccaaacctgc attaaaaatt tcggttgggg
     2821 cgacctcgga gcagaaccca acctccgagc agtacatgct aagacttcac cagtcaaagc
     2881 gaactactat actcaattga tccaataact tgaccaacgg aacaagttac cctagggata
     2941 acagcgcaat cctattctag agtccatatc aacaataggg tttacgacct cgatgttgga
     3001 tcaggacatc ccgatggtgc agccgctatt aaaggttcgt ttgttcaacg attaaagtcc
     3061 tacgtgatct gagttcagac cggagtaatc caggtcggtt tctatctact tcaaattcct
     3121 ccctgtacga aaggacaaga gaaataaggc ctacttcaca aagcgccttc ccccgtaaat
     3181 gatatcatct caacttagta ttatacccac acccacccaa gaacagggtt tgttaagatg
     3241 gcagagcccg gtaatcgcat aaaacttaaa actttacagt cagaggttca attcctcttc
     3301 ttaacaacat acccatggcc aacctcctac tcctcattgt acccattcta atcgcaatgg
     3361 cattcctaat gcttaccgaa cgaaaaattc taggctatat acaactacgc aaaggcccca
     3421 acgttgtagg cccctacggg ctactacaac ccttcgctga cgccataaaa ctcttcacca
     3481 aagagcccct aaaacccgcc acatctacca tcaccctcta catcaccgcc ccgaccttag
     3541 ctctcaccat cgctcttcta ctatgaaccc ccctccccat acccaacccc ctggtcaacc
     3601 tcaacctagg cctcctattt attctagcca cctctagcct agccgtttac tcaatcctct
     3661 gatcagggtg agcatcaaac tcaaactacg ccctgatcgg cgcactgcga gcagtagccc
     3721 aaacaatctc atatgaagtc accctagcca tcattctact atcaacatta ctaataagtg
     3781 gctcctttaa cctctccacc cttatcacaa cacaagaaca cctctgatta ctcctgccat
     3841 catgaccctt ggccataata tgatttatct ccacactagc agagaccaac cgaaccccct
     3901 tcgaccttgc cgaaggggag tccgaactag tctcaggctt caacatcgaa tacgccgcag
     3961 gccccttcgc cctattcttc atagccgaat acacaaacat tattataata aacaccctca
     4021 ccactacaat cttcctagga acaacatatg acgcactctc ccctgaactc tacacaacat
     4081 attttgtcac caagacccta cttctaacct ccctgttctt atgaattcga acagcatacc
     4141 cccgattccg ctacgaccaa ctcatacacc tcctatgaaa aaacttccta ccactcaccc
     4201 tagcattact tatatgatat gtctccatac ccattacaat ctccagcatt ccccctcaaa
     4261 cctaagaaat atgtctgata aaagagttac tttgatagag taaataatag gagcttaaac
     4321 ccccttattt ctaggactat gagaatcgaa cccatccctg agaatccaaa attctccgtg
     4381 ccacctatca caccccatcc taaagtaagg tcagctaaat aagctatcgg gcccataccc
     4441 cgaaaatgtt ggttataccc ttcccgtact aattaatccc ctggcccaac ccgtcatcta
     4501 ctctaccatc tttgcaggca cactcatcac agcgctaagc tcgcactgat tttttacctg
     4561 agtaggccta gaaataaaca tgctagcttt tattccagtt ctaaccaaaa aaataaaccc
     4621 tcgttccaca gaagctgcca tcaagtattt cctcacgcaa gcaaccgcat ccataatcct
     4681 tctaatagct atcctcttca acaatatact ctccggacaa tgaaccataa ccaatactac
     4741 caatcaatac tcatcattaa taatcataat ggctatagca ataaaactag gaatagcccc
     4801 ctttcacttc tgagtcccag aggttaccca aggcacccct ctgacatccg gcctgcttct
     4861 tctcacatga caaaaactag cccccatctc aatcatatac caaatctctc cctcactaaa
     4921 cgtaagcctt ctcctcactc tctcaatctt atccatcata gcaggcagtt gaggtggatt
     4981 aaaccaaacc cagctacgca aaatcttagc atactcctca attacccaca taggatgaat
     5041 aatagcagtt ctaccgtaca accctaacat aaccattctt aatttaacta tttatattat
     5101 cctaactact accgcattcc tactactcaa cttaaactcc agcaccacga ccctactact
     5161 atctcgcacc tgaaacaagc taacatgact aacaccctta attccatcca ccctcctctc
     5221 cctaggaggc ctgcccccgc taaccggctt tttgcccaaa tgggccatta tcgaagaatt
     5281 cacaaaaaac aatagcctca tcatccccac catcatagcc accatcaccc tccttaacct
     5341 ctacttctac ctacgcctaa tctactccac ctcaatcaca ctactcccca tatctaacaa
     5401 cgtaaaaata aaatgacagt ttgaacatac aaaacccacc ccattcctcc ccacactcat
     5461 cgcccttacc acgctactcc tacctatctc cccttttata ctaataatct tatagaaatt
     5521 taggttaaat acagaccaag agccttcaaa gccctcagta agttgcaata cttaatttct
     5581 gtaacagcta aggactgcaa aaccccactc tgcatcaact gaacgcaaat cagccacttt
     5641 aattaagcta agcccttact agaccaatgg gacttaaacc cacaaacact tagttaacag
     5701 ctaagcaccc taatcaactg gcttcaatct acttctcccg ccgccgggaa aaaaggcggg
     5761 agaagccccg gcaggtttga agctgcttct tcgaatttgc aattcaatat gaaaatcacc
     5821 tcggagctgg taaaaagagg cctaacccct gtctttagat ttacagtcca atgcttcact
     5881 cagccatttt acctcacccc cactgatgtt cgccgaccgt tgactattct ctacaaacca
     5941 caaagacatt ggaacactat acctattatt cggcgcatga gctggagtcc taggcacagc
     6001 tctaagcctc cttattcgag ccgagctggg ccagccaggc aaccttctag gtaacgacca
     6061 catctacaac gttatcgtca cagcccatgc atttgtaata atcttcttca tagtaatacc
     6121 catcataatc ggaggctttg gcaactgact agttccccta ataatcggtg cccccgatat
     6181 ggcgtttccc cgcataaaca acataagctt ctgactctta cctccctctc tcctactcct
     6241 gctcgcatct gctatagtgg aggccggagc aggaacaggt tgaacagtct accctccctt
     6301 agcagggaac tactcccacc ctggagcctc cgtagaccta accatcttct ccttacacct
     6361 agcaggtgtc tcctctatct taggggccat caatttcatc acaacaatta tcaatataaa
     6421 accccctgcc ataacccaat accaaacacc cctcttcgtc tgatccgtcc taatcacagc
     6481 agtcctactt ctcctatctc tcccagtcct agctgctggc atcactatac tactaacaga
     6541 ccgcaacctc aacaccacct tcttcgaccc cgccggagga ggagacccca ttctatacca
     6601 acacctattc tgatttttcg gtcaccctga agtttatatt cttatcctac caggcttcgg
     6661 aataatctcc cacattgtaa cctactactc cggaaaaaaa gaaccatttg gatacatagg
     6721 tatggtctga gctatgatat caattggctt cctagggttt atcgtgtgag cacaccatat
     6781 atttacagta ggaatagacg tagacacacg agcatatttc acctccgcta ccataatcat
     6841 cgctatcccc accggcgtca aagtatttag ctgactcgcc acactccacg gaagcaatat
     6901 gaaatgatct gctgcagtgc tctgagccct aggattcatc tttcttttca ccgtaggtgg
     6961 cctgactggc attgtattag caaactcatc actagacatc gtactacacg acacgtacta
     7021 cgttgtagct cacttccact atgtcctatc aataggagct gtatttgcca tcataggagg
     7081 cttcattcac tgatttcccc tattctcagg ctacacccta gaccaaacct acgccaaaat
     7141 ccatttcact atcatattca tcggcgtaaa tctaactttc ttcccacaac actttctcgg
     7201 cctatccgga atgccccgac gttactcgga ctaccccgat gcatacacca catgaaacat
     7261 cctatcatct gtaggctcat tcatttctct aacagcagta atattaataa ttttcatgat
     7321 ttgagaagcc ttcgcttcga agcgaaaagt cctaatagta gaagaaccct ccataaacct
     7381 ggagtgacta tatggatgcc ccccacccta ccacacattc gaagaacccg tatacataaa
     7441 atctagacaa aaaaggaagg aatcgaaccc cccaaagctg gtttcaagcc aaccccatgg
     7501 cctccatgac tttttcaaaa aggtattaga aaaaccattt cataactttg tcaaagttaa
     7561 attataggct aaatcctata tatcttaatg gcacatgcag cgcaagtagg tctacaagac
     7621 gctacttccc ctatcataga agagcttatc acctttcatg atcacgccct cataatcatt
     7681 ttccttatct gcttcctagt cctgtatgcc cttttcctaa cactcacaac aaaactaact
     7741 aatactaaca tctcagacgc tcaggaaata gaaaccgtct gaactatcct gcccgccatc
     7801 atcctagtcc tcatcgccct cccatcccta cgcatccttt acataacaga cgaggtcaac
     7861 gatccctccc ttaccatcaa atcaattggc caccaatggt actgaaccta cgagtacacc
     7921 gactacggcg gactaatctt caactcctac atacttcccc cattattcct agaaccaggc
     7981 gacctgcgac tccttgacgt tgacaatcga gtagtactcc cgattgaagc ccccattcgt
     8041 ataataatta catcacaaga cgtcttgcac tcatgagctg tccccacatt aggcttaaaa
     8101 acagatgcaa ttcccggacg tctaaaccaa accactttca ccgctacacg accgggggta
     8161 tactacggtc aatgctctga aatctgtgga gcaaaccaca gtttcatgcc catcgtccta
     8221 gaattaattc ccctaaaaat ctttgaaata gggcccgtat ttaccctata gcaccccctc
     8281 taccccctct agagcccact gtaaagctaa cttagcatta accttttaag ttaaagatta
     8341 agagaaccaa cacctcttta cagtgaaatg ccccaactaa atactaccgt atggcccacc
     8401 ataattaccc ccatactcct tacactattc ctcatcaccc aactaaaaat attaaacaca
     8461 aactaccacc tacctccctc accaaagccc ataaaaataa aaaattataa caaaccctga
     8521 gaaccaaaat gaacgaaaat ctgttcgctt cattcattgc ccccacaatc ctaggcctac
     8581 ccgccgcagt actgatcatt ctatttcccc ctctattgat ccccacctcc aaatatctca
     8641 tcaacaaccg actaatcacc acccaacaat gactaatcaa actaacctca aaacaaatga
     8701 tagccataca caacactaaa ggacgaacct gatctcttat actagtatcc ttaatcattt
     8761 ttattgccac aactaacctc ctcggactcc tgcctcactc atttacacca accacccaac
     8821 tatctataaa cctagccatg gccatcccct tatgagcggg cgcagtgatt ataggctttc
     8881 gctctaagat taaaaatgcc ctagcccact tcttaccaca aggcacacct acacccctta
     8941 tccccatact agttattatc gaaaccatca gcctactcat tcaaccaata gccctggccg
     9001 tacgcctaac cgctaacatt actgcaggcc acctactcat gcacctaatt ggaagcgcca
     9061 ccctagcaat atcaaccatt aaccttccct ctacacttat catcttcaca attctaattc
     9121 tactgactat cctagaaatc gctgtcgcct taatccaagc ctacgttttc acacttctag
     9181 taagcctcta cctgcacgac aacacataat gacccaccaa tcacatgcct atcatatagt
     9241 aaaacccagc ccatgacccc taacaggggc cctctcagcc ctcctaatga cctccggcct
     9301 agccatgtga tttcacttcc actccataac gctcctcata ctaggcctac taaccaacac
     9361 actaaccata taccaatgat ggcgcgatgt aacacgagaa agcacatacc aaggccacca
     9421 cacaccacct gtccaaaaag gccttcgata cgggataatc ctatttatta cctcagaagt
     9481 ttttttcttc gcaggatttt tctgagcctt ttaccactcc agcctagccc ctacccccca
     9541 actaggaggg cactggcccc caacaggcat caccccgcta aatcccctag aagtcccact
     9601 cctaaacaca tccgtattac tcgcatcagg agtatcaatc acctgagctc accatagtct
     9661 aatagaaaac aaccgaaacc aaataattca agcactgctt attacaattt tactgggtct
     9721 ctattttacc ctcctacaag cctcagagta cttcgagtct cccttcacca tttccgacgg
     9781 catctacggc tcaacatttt ttgtagccac aggcttccac ggacttcacg tcattattgg
     9841 ctcaactttc ctcactatct gcttcatccg ccaactaata tttcacttta catccaaaca
     9901 tcactttggc ttcgaagccg ccgcctgata ctggcatttt gtagatgtgg tttgactatt
     9961 tctgtatgtc tccatctatt gatgagggtc ttactctttt agtataaata gtaccgttaa
    10021 cttccaatta actagttttg acaacattca aaaaagagta ataaacttcg ccttaatttt
    10081 aataatcaac accctcctag ccttactact aataattatt acattttgac taccacaact
    10141 caacggctac atagaaaaat ccacccctta cgagtgcggc ttcgacccta tatcccccgc
    10201 ccgcgtccct ttctccataa aattcttctt agtagctatt accttcttat tatttgatct
    10261 agaaattgcc ctccttttac ccctaccatg agccctacaa acaactaacc tgccactaat
    10321 agttatgtca tccctcttat taatcatcat cctagcccta agtctggcct atgagtgact
    10381 acaaaaagga ttagactgag ctgaattggt atatagttta aacaaaacga atgatttcga
    10441 ctcattaaat tatgataatc atatttacca aatgcccctc atttacataa atattatact
    10501 agcatttacc atctcacttc taggaatact agtatatcgc tcacacctca tatcctccct
    10561 actatgccta gaaggaataa tactatcgct gttcattata gctactctca taaccctcaa
    10621 cacccactcc ctcttagcca atattgtgcc tattgccata ctagtctttg ccgcctgcga
    10681 agcagcggtg ggcctagccc tactagtctc aatctccaac acatatggcc tagactacgt
    10741 acataaccta aacctactcc aatgctaaaa ctaatcgtcc caacaattat attactacca
    10801 ctgacatgac tttccaaaaa acacataatt tgaatcaaca caaccaccca cagcctaatt
    10861 attagcatca tccccctact attttttaac caaatcaaca acaacctatt tagctgttcc
    10921 ccaacctttt cctccgaccc cctaacaacc cccctcctaa tactaactac ctgactccta
    10981 cccctcacaa tcatggcaag ccaacgccac ttatccagtg aaccactatc acgaaaaaaa
    11041 ctctacctct ctatactaat ctccctacaa atctccttaa ttataacatt cacagccaca
    11101 gaactaatca tattttatat cttcttcgaa accacactta tccccacctt ggctatcatc
    11161 acccgatgag gcaaccagcc agaacgcctg aacgcaggca catacttcct attctacacc
    11221 ctagtaggct cccttcccct actcatcgca ctaatttaca ctcacaacac cctaggctca
    11281 ctaaacattc tactactcac tctcactgcc caagaactat caaactcctg agccaacaac
    11341 ttaatatgac tagcttacac aatagctttt atagtaaaga tacctcttta cggactccac
    11401 ttatgactcc ctaaagccca tgtcgaagcc cccatcgctg ggtcaatagt acttgccgca
    11461 gtactcttaa aactaggcgg ctatggtata atacgcctca cactcattct caaccccctg
    11521 acaaaacaca tagcctaccc cttccttgta ctatccctat gaggcataat tataacaagc
    11581 tccatctgcc tacgacaaac agacctaaaa tcgctcattg catactcttc aatcagccac
    11641 atagccctcg tagtaacagc cattctcatc caaaccccct gaagcttcac cggcgcagtc
    11701 attctcataa tcgcccacgg acttacatcc tcattactat tctgcctagc aaactcaaac
    11761 tacgaacgca ctcacagtcg catcataatc ctctctcaag gacttcaaac tctactccca
    11821 ctaatagctt tttgatgact tctagcaagc ctcgctaacc tcgccttacc ccccactatt
    11881 aacctactgg gagaactctc tgtgctagta accacgttct cctgatcaaa tatcactctc
    11941 ctacttacag gactcaacat actagtcaca gccctatact ccctctacat atttaccaca
    12001 acacaatggg gctcactcac ccaccacatt aacaacataa aaccctcatt cacacgagaa
    12061 aacaccctca tgttcataca cctatccccc attctcctcc tatccctcaa ccccgacatc
    12121 attaccgggt tttcctcttg taaatatagt ttaaccaaaa catcagattg tgaatctgac
    12181 aacagaggct tacgacccct tatttaccga gaaagctcac aagaactgct aactcatgcc
    12241 cccatgtcta acaacatggc tttctcaact tttaaaggat aacagctatc cattggtctt
    12301 aggccccaaa aattttggtg caactccaaa taaaagtaat aaccatgcac actactataa
    12361 ccaccctaac cctgacttcc ctaattcccc ccatccttac caccttcgtt aaccccaaca
    12421 aaaaaaactc atacccccat tatgtaaaat ccattgtcgc atccaccttt attatcagtc
    12481 tcttccccac aacgatattc atgtgcctag accaagaagt tattatctcg aactgacact
    12541 gagccacaac ccaaacaacc cagctctccc taagcttcaa actagactac ttctccataa
    12601 tattcatccc tgtagcattg ttcgttacat ggtccatcat agaattctca ctgtgatata
    12661 taaactcaga cccaaacatt aatcagttct tcaaatatct actcattttc ctaattacca
    12721 tactaatctt agttaccgct aacaacctat tccaactgtt catcggctga gagggcgtag
    12781 gaattatatc cttcttgctc atcagttgat gatacgcccg agcagatgcc aacacagcag
    12841 ccattcaagc aatcctatac aaccgtatcg gcgatatcgg tttcatcctc gccttagcat
    12901 gatttatcct acactccaac tcatgagacc cacaacaaat agcccttcta accgctaatc
    12961 caagcctcac cccactacta ggcctcctcc tagcagcagc aggcaaatca gcccaattag
    13021 gtctccaccc ctgactcccc tcagccatag aaggccccac cccagtctca gccctactcc
    13081 actcaagcac tatagttgta gcaggaatct tcttactcat ccgcttccac cccctagcag
    13141 aaaatagccc actaatccaa actctaacac tatgcttagg cgctatcacc actctgttcg
    13201 cagcagtctg cgcccttaca caaaatgaca tcaaaaaaat cgtagccttc tccacttcaa
    13261 gtcaactagg actcataata gttacaatcg gcatcaacca accacaccta gcattcctgc
    13321 acatctgtac ccacgccttc ttcaaagcca tactatttat gtgctccggg tccatcatcc
    13381 acaaccttaa caatgaacaa gatattcgaa aaataggagg actactcaaa accatacctc
    13441 tcacttcaac ctccctcacc attggcagcc tagcattagc aggaatacct ttcctcacag
    13501 gtttctactc caaagaccac atcatcgaaa ccgcaaacat atcatacaca aacgcctgag
    13561 ccctatctat tactctcatc gctacctccc tgacaagcgc ctatagcact cgaataattc
    13621 ttctcaccct aacaggtcga cctcgcttcc ccacccttac taacattaac gaaaataacc
    13681 ccaccctact aaaccccatt aaacgcctgg cagccggaag cctattcgca ggatttctca
    13741 ttactaacaa catttccccc gcatccccct tccaaacaac aatccccctc tacctaaaac
    13801 tcacagccct cgctgtcact ttcctaggac ttctaacagc cctagacctc aactacctaa
    13861 ccaacaaact taaaataaaa tccccactat gcacatttta tttctccaac atactcggat
    13921 tctaccctag catcacacac cgcacaatcc cctatctagg ccttcttacg agccaaaacc
    13981 tgcccctact cctcctagac ctaacctgac tagaaaagct attacctaaa acaatttcac
    14041 agcaccaaat ctccacctcc atcatcacct caacccaaaa aggcataatt aaactttact
    14101 tcctctcttt cctcttccca ctcatcctaa ccctactcct aatcacataa cctattcccc
    14161 cgagcaatct caattacaat atatacacca acaaacaatg ttcaaccagt aactactact
    14221 aatcaacgcc cataatcata caaagccccc gcaccaatag gatcctcccg aatcaaccct
    14281 gacccctctc cttcataaat tattcagctt cctacactat taaagtttac cacaaccacc
    14341 accccatcat actctttcac ccacagcacc aatcctacct ccatcgctaa ccccactaaa
    14401 acactcacca agacctcaac ccctgacccc catgcctcag gatactcctc aatagccatc
    14461 gctgtagtat atccaaagac aaccatcatt ccccctaaat aaattaaaaa aactattaaa
    14521 cccatataac ctcccccaaa attcagaata ataacacacc cgaccacacc gctaacaatc
    14581 aatactaaac ccccataaat aggagaaggc ttagaagaaa accccacaaa ccccattact
    14641 aaacccacac tcaacagaaa caaagcatac atcattattc tcgcacggac tacaaccacg
    14701 accaatgata tgaaaaacca tcgttgtatt tcaactacaa gaacaccaat gaccccaata
    14761 cgcaaaatta accccctaat aaaactaatt aaccactcat tcatcgacct ccccacccca
    14821 tccaacatct ccgcatgatg aaacttcggc tcactccttg gcgcctgcct gatcctccaa
    14881 atcaccacag gactattcct agccatgcac tactcaccag acgcctcaac cgccttttca
    14941 tcaatcgccc acatcactcg agacgtaaat tatggctgaa tcatccgcta ccttcacgcc
    15001 aatggcgcct caatattctt tatctgcctc ttcctacaca tcggacgagg cctatattac
    15061 ggatcatttc tctactcaga aacctgaaac atcggcatta tcctcctgct tgcaactata
    15121 gcaacagcct tcataggcta tgtcctcccg tgaggccaaa tatcattctg aggggccaca
    15181 gtaattacaa acttactatc cgccatccca tacattggga cagacctagt tcaatgaatc
    15241 tgaggaggct actcagtaga cagtcccacc ctcacacgat tctttacctt tcacttcatc
    15301 ttacccttca ttattgcagc cctagcagca ctccacctcc tattcttgca cgaaacggga
    15361 tcaaacaacc ccctaggaat cacctcccat tccgataaaa tcaccttcca cccttactac
    15421 acaatcaaag acgccctcgg cttacttctc ttccttctct ccttaatgac attaacacta
    15481 ttctcaccag acctcctagg cgacccagac aattataccc cagccaaccc cttaaacacc
    15541 cctccccaca tcaagcccga atgatatttc ctattcgcct acacaattct ccgatccgtc
    15601 cctaacaaac taggaggcgt ccttgcccta ttactatcca tcctcatcct agcaataatc
    15661 cccatcctcc atatatccaa acaacaaagc ataatatttc gcccactaag ccaatcactt
    15721 tattgactcc tagccgcaga cctcctcatt ctaacctgaa tcggaggaca accagtaagc
    15781 taccctttta ccatcattgg acaagtagca tccgtactat acttcacaac aatcctaatc
    15841 ctaataccaa ctatctccct aattgaaaac aaaatactca aatgggcctg tccttgtagt
    15901 ataaactaat acaccagtct tgtaaaccgg agatgaaaac ctttttccaa ggacaaatca
    15961 gagaaaaagt ctttaactcc accattagca cccaaagcta agattctaat ttaaactatt
    16021 ctctgttctt tcatggggaa gcagatttgg gtaccaccca agtattgact cacccatcaa
    16081 caaccgctat gtatttcgta cattactgcc agccaccatg aatattgtac agtaccataa
    16141 atacttgacc acctgtagta cataaaaacc caatccacat caaccccccc cccccatgct
    16201 tacaagcaag tacagcaatc aaccctcaac tatcacacat caactgcaac cccaaagcca
    16261 cccctcaccc actaggatac caacaaacct acccaccctt aacagtacat agcacataaa
    16321 gccatttacc gtacatagca cattacagtc aaatcccttc tcgtccccat ggatgacccc
    16381 cctcagatag gggtcccttg accaccatcc tccgtgaaat caatatcccg cacaagagtg
    16441 ctactctcct cgctccgggc ccataacact tgggggtagc taaagtgaac tgtatccgac
    16501 atctggttcc tacttcaggg ccataaagcc taaatagccc acacgttccc cttaaataag
    16561 acatcacgat g
//

