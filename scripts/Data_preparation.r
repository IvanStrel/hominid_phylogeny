require(rworldmap)
Map <- getMap()
Map <- Map[-7,]

data_files <- list.files("../initial_data", recursive = TRUE)
data_files <- paste("../initial_data/", data_files, sep="")
GB <- grepl("gb$",data_files)
Country <- gsub(".fasta", "", data_files[!GB])
Country <- regmatches(Country,gregexpr("[^_]+$", Country, perl = TRUE))
Country <- as.character(Country)
Country[length(Country)]<-"NA"

# Get indexes of each species
Homo_sapience_o <- grep("/Homo_sapiens/",
	data_files[!GB])
Homo_heidelbergensis_o <- grep("/Homo_heidelbergensis",
	data_files[!GB])
Homo_sapiens_neanderthalensis_o <- grep("/Homo_sapiens_neanderthalensis",
	data_files[!GB])
Homo_sapiens_ssp_Denisova_o <- grep("/Homo_sapiens_ssp._Denisova",
	data_files[!GB])
Pan_troglodytes_troglodytes_o <- grep("/Pan_troglodytes_troglodytes",
	data_files[!GB])
Pan_troglodytes_schweinfurthii_o <- grep("/Pan_troglodytes_schweinfurthii",
	data_files[!GB])
Pan_troglodytes_ellioti_o <- grep("/Pan_troglodytes_ellioti",
	data_files[!GB])
Pan_troglodytes_verus_o <- grep("/Pan_troglodytes_verus",
	data_files[!GB])

# Replace invalid country names
C_missed <- c("Palestine", "Viet Nam", "England", "USA", "Tanzania", "Cote d'Ivoire")
C_replace <- c("Israel", "Vietnam","United Kingdom",
	"United States of America", "United Republic of Tanzania", "Ivory Coast")
Country[which(Country %in% C_missed)] <- C_replace

# Map production
	# Prepare map dimensions
Bbox <- Map@bbox
Xlim <- Bbox[1, 1:2]
Ylim <- Bbox[2, 1:2]
aspect <- (Ylim[2] - Ylim[1]) / (Xlim[2] - Xlim[1])
	#Homo sapiens
Oc <- Map@data$ADMIN %in% Country[Homo_sapience_o]
png("../maps_output/Homo_sapiens_sapiens.png",
	height=10 * aspect, width=10, unit="in", res=300, bg="grey")
par(mar=c(0,0,0,0))
plot(NA, xlim=Xlim, ylim=Ylim, xaxt="n", yaxt="n", bty="n", bg="grey")
plot(Map, col=1, bord="lightgray", add=TRUE)
Oc_coord <- coordinates(Map[Oc,])
points(Oc_coord, col=2, pch=19, cex=1.25)
legend("bottomleft",
	legend=c("H. sapiens sapiens"),
	pt.bg = c(2), pch = 21, title = "Homo sapiens", ins = 0.01, pt.cex=1.5)
dev.off()

	#Pan troglodytes
Oc1 <- Map@data$ADMIN %in% Country[Pan_troglodytes_troglodytes_o]
Oc2 <- Map@data$ADMIN %in% Country[Pan_troglodytes_schweinfurthii_o]
Oc3 <- Map@data$ADMIN %in% Country[Pan_troglodytes_ellioti_o]
Oc4 <- Map@data$ADMIN %in% Country[Pan_troglodytes_verus_o]
png("../maps_output/Pan_troglodytes.png",
	height=10 * aspect, width=10, unit="in", res=300, bg="grey")
par(mar=c(0,0,0,0))
plot(NA, xlim=Xlim, ylim=Ylim, xaxt="n", yaxt="n", bty="n", bg="grey")
plot(Map, col=1, bord="lightgray", add=TRUE)
Oc_coord <- coordinates(Map[Oc1, ]) - c(2, 0)
points(Oc_coord, col=2, pch=19, cex=1.25)
Oc_coord <- coordinates(Map[Oc2, ]) + c(2, 0)
points(Oc_coord, col=3, pch=19, cex=1.25)
Oc_coord <- coordinates(Map[Oc3, ]) + c(0, 2)
points(Oc_coord, col=4, pch=19, cex=1.25)
Oc_coord <- coordinates(Map[Oc4, ]) - c(0, 2)
points(Oc_coord, col=7, pch=19, cex=1.25)
legend("bottomleft",
	legend=c("P. troglodytes troglodytes", "P. troglodytes schweinfurthii",
	"P. troglodytes ellioti", "P. troglodytes verus"),
	pt.bg = c(2, 3, 4, 7), pch = 21, title = "Pan troglodytes", ins = 0.01, pt.cex=1.5)
dev.off()

	#Pre humans
Oc1 <- Map@data$ADMIN %in% Country[Homo_heidelbergensis_o]
Oc2 <- Map@data$ADMIN %in% Country[Homo_sapiens_neanderthalensis_o]
Oc3 <- Map@data$ADMIN %in% Country[Homo_sapiens_ssp_Denisova_o]
png("../maps_output/Human_ancestors.png",
	height=10 * aspect, width=10, unit="in", res=300, bg="grey")
par(mar=c(0,0,0,0))
plot(NA, xlim=Xlim, ylim=Ylim, xaxt="n", yaxt="n", bty="n", bg="grey")
plot(Map, col=1, bord="lightgray", add=TRUE)
Oc_coord <- coordinates(Map[Oc1, ]) - c(2, 0)
points(Oc_coord, col=2, pch=19, cex=1.25)
Oc_coord <- coordinates(Map[Oc2, ]) + c(2, 0)
points(Oc_coord, col=3, pch=19, cex=1.25)
Oc_coord <- coordinates(Map[Oc3, ]) + c(0, 2)
points(Oc_coord, col=7, pch=19, cex=1.25)
legend("bottomleft",
	legend=c("H. heidelbergensis", "H. sapiens neanderthalensis",
	"H. sapiens ssp. Denisova"),
	pt.bg = c(2, 3, 7), pch = 21, title = "Homo sapiens ancestors", ins = 0.01, pt.cex=1.5)
dev.off()

# Prepare FASTA composite
	# Read Gen bank files
GenBank_text <- sapply(data_files[GB], function(x){
	con <- file(x)
	Lines <- readLines(con)
	close(con)
	return(Lines)
})
	# Get species name
Species <- lapply(GenBank_text, function(x){
	Spec <- x[grepl("ORGANISM", x)]
	Spec <- gsub("ORGANISM", "",Spec)
	Spec <- gsub("  ", "",Spec)
	Spec <- as.character(strsplit(Spec, " ")[[1]])
	Spec <- paste(paste(substring(Spec[1], 1, 1), ".", sep=""), paste(Spec[-1], collapse=" "))
})
Species <- as.character(Species)
	# Write Species names to CSV
save(Species, file = "..//preparation_output//Species.RData")
	# Add number to repetitive species names
Species_id <- Species
Species_num <- Species
Spec_un <- unique(Species)
for (Spec in Spec_un){
	Mask <- Species == Spec
	Mask_l <- sum(Mask)
	if (Mask_l > 1){
		Species_num[Mask] <- paste(Species[Mask], c(1:Mask_l))
	}
}
Species_id <- sprintf("%02d", 1:length(Species))
	
	# Read fasta
Fasta_text <- sapply(data_files[!GB], function(x){
	con <- file(x)
	Lines <- readLines(con)
	close(con)
	return(Lines)
}, simplify = FALSE)

# Write composite FASTA
File <- "../preparation_output/composite_fasta.fasta"
if (file.exists(File)) file.remove(File)
for (i in 1:length(Fasta_text)){
	fileConn<-file(File, open = "at")
	Text <- Fasta_text[[i]]
	Text[1] <- paste(">", Species_id[i])
	writeLines(Text, fileConn)
	Fasta_text[[i]] <- Text
	close(fileConn)
}


