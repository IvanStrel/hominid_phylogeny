#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#	Script for phylogenetic trees
#			analysis.
# Require results from Data_preparation.r
# particular species names var:Species.
#
# Date:		26.03.2015
# Author:	Ivan I. Strel'nikov
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

require(dendextend)

# Predefine species
mod_humans <- c(2:23)
neandertal <- c(24:26)
ancestors <- c(1, 27)
pan_troglodites <- c(28:39)
# Load species names strings
load("..//preparation_output//Species.RData")
# Add number to species names
Species_num <- Species
Spec_un <- unique(Species)
for (Spec in Spec_un){
	Mask <- Species == Spec
	Mask_l <- sum(Mask)
	if (Mask_l > 1){
		Species_num[Mask] <- paste(Species[Mask], c(1:Mask_l))
	}
}

# Get distance matrix
dist_tab <- read.csv("..//output//RAxML_bipartitions.csv",
	header=FALSE, as.is=TRUE, sep=" ")
# Clean distance matrix
dist_tab <- dist_tab[, !is.na(dist_tab[1, ])]
row_names <- dist_tab[, 1]
dist_tab <- as.matrix(dist_tab[, -1])
rownames(dist_tab) <- row_names
# Prepare 
dist_mat <- as.dist(dist_tab)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##### Modern humans and tree demo #######
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
spec_mask <- as.numeric(row_names) %in% mod_humans
spec_labs <- Species_num[as.numeric(row_names)[spec_mask]]
dist_mat_mh <- dist_tab[spec_mask, spec_mask]
rownames(dist_mat_mh) <- spec_labs
dist_mat_mh <- as.dist(dist_mat_mh)
	# Create dendrogram
dend_mh <- as.dendrogram(hclust(dist_mat_mh))
	### Whole dendrogramm ###
png("..//dendrograms_output//homo_sapiense_whole_tree.png",
	width = 8.27, height = 11.7 * (2 / 3), unit="in", res=300)
par(mar=c(2.1, 1.1, 2, 8))
dend_mh %>% set("nodes_pch", 19) %>%
			set("leaves_pch", 19) %>%
			set("leaves_cex", 1) %>%
			set("leaves_col", 4) %>%
			set("branches_lwd", 2) %>%
			set("labels_cex", 1.5) %>%
			plot(horiz = TRUE, main = "Homo sapiens sapiens phylogeny",
				cex.axis=1.5, cex.main=1.5)
dev.off()
	### Explanatory dendrogramm ###
# Get colors
shortes_path <- function(sub_dend, the_labels){
	xor(any(labels(sub_dend) %in% the_labels[1]), any(labels(sub_dend) %in% the_labels[2]))
}
cols1 <- noded_with_condition(dend_mh, shortes_path,
            the_labels = c("H. sapiens 7", "H. sapiens 8")) %>% 
            ifelse(2,0)
cols2 <- noded_with_condition(dend_mh, shortes_path,
            the_labels = c("H. sapiens 4", "H. sapiens 3")) %>% 
            ifelse(3,0)
cols <- cols1 + cols2
cols[cols == 0] <- 1
cols[cols == 3] <- "forestgreen"
edge_lty <- rep(3, length(cols))
edge_lty[cols > 1] <- 1
edge_lwd <- edge_lty
edge_lwd[edge_lty == 1] <- 3
edge_lwd[edge_lty != 1] <- 2

png("..//dendrograms_output//homo_sapiense_explonatory_tree.png",
	width = 8.27, height = 11.7 * (2 / 3), unit="in", res=300)
par(mar=c(2.1, 1.1, 2, 8))			
dend_mh %>% set("nodes_pch", 19) %>%
			set("leaves_pch", 19) %>%
			set("leaves_cex", 1) %>%
			set("leaves_col", 4) %>%
			set("branches_lwd", 2) %>%
			set("branches_col", cols) %>%
			set("branches_lty", edge_lty) %>%
			set("branches_lwd", edge_lwd) %>%
			set("labels_cex", 1.5) %>%
			plot(horiz = TRUE, main = "Homo sapiens sapiens phylogeny",
				cex.axis=1.5, cex.main=1.5)
abline(v=c(0.0026920020, 0.0006308039), col = c(2, "forestgreen"), lty=2, lwd=2)			
dev.off()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##### Modern humans and Neanderthal #######
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
spec_mask <- as.numeric(row_names) %in% c(mod_humans, neandertal)
spec_labs <- Species_num[as.numeric(row_names)[spec_mask]]
dist_mat_hn <- dist_tab[spec_mask, spec_mask]
rownames(dist_mat_hn) <- spec_labs
dist_mat_hn <- as.dist(dist_mat_hn)
	# Create dendrogram
dend_nh <- as.dendrogram(hclust(dist_mat_hn))

png("..//dendrograms_output//homo_plus_neanderthal_tree.png",
	width = 8.27, height = 11.7 * (2 / 3), unit="in", res=300)
par(mar=c(2.1, 1.1, 2.4, 12))			
dend_nh %>% set("nodes_pch", 19) %>%
			set("leaves_pch", 19) %>%
			set("leaves_cex", 1) %>%
			set("leaves_col", 4) %>%
			set("branches_lwd", 2) %>%
			set("branches_lwd", 2) %>%
			set("labels_cex", 1) %>%
			set("branches_k_color", value = c("firebrick2", "darkorchid4"), k = 2) %>%
			plot(horiz = TRUE, main = "sapiens and neanderthalensis\nphylogeny",
				cex.axis=1.5, cex.main=1.5)
legend("topleft", legend=c("H. sapiens neanderthalensis", "H. sapiens sapiens"),
	fill = c("firebrick2", "darkorchid4"), ins = 0.05, bty="n")
dev.off()

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##### Humans and ancestral #######
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
spec_mask <- as.numeric(row_names) %in% c(mod_humans, neandertal, ancestors)
spec_labs <- Species_num[as.numeric(row_names)[spec_mask]]
dist_mat_ha <- dist_tab[spec_mask, spec_mask]
rownames(dist_mat_ha) <- spec_labs
dist_mat_ha <- as.dist(dist_mat_ha)
	# Create dendrogram
dend_ha <- as.dendrogram(hclust(dist_mat_ha))

png("..//dendrograms_output//homo_plus_ancestors_tree.png",
	width = 8.27, height = 11.7 * (2 / 3), unit="in", res=300)
par(mar=c(2.1, 1.1, 2.4, 12))			
dend_ha %>% set("nodes_pch", 19) %>%
			set("leaves_pch", 19) %>%
			set("leaves_cex", 1) %>%
			set("leaves_col", 4) %>%
			set("branches_lwd", 2) %>%
			set("branches_lwd", 2) %>%
			set("labels_cex", 1) %>%
			set("branches_k_color", value = c("forestgreen", "firebrick2", "darkorchid4"), k = 3) %>%
			plot(horiz = TRUE, main = "sapiens and ancestors\nphylogeny",
				cex.axis=1.5, cex.main=1.5)
legend("topleft", legend=c("Homo (ancestors)", "H. sapiens neanderthalensis", "H. sapiens sapiens"),
	fill = c("forestgreen", "firebrick2", "darkorchid4"), ins = 0.05, bty="n")
dev.off()


#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##### Homo and chimps #######
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
spec_mask <- as.numeric(row_names) %in% c(mod_humans, neandertal, ancestors, pan_troglodites)
spec_labs <- Species_num[as.numeric(row_names)[spec_mask]]
dist_mat_ha <- dist_tab[spec_mask, spec_mask]
rownames(dist_mat_ha) <- spec_labs
dist_mat_ha <- as.dist(dist_mat_ha)
	# Create dendrogram
dend_ha <- as.dendrogram(hclust(dist_mat_ha))

png("..//dendrograms_output//homo_plus_Pan_tree.png",
	width = 8.27, height = 11.7 * (2 / 3), unit="in", res=300)
par(mar=c(2.1, 1.1, 2.4, 12))			
dend_ha %>% set("nodes_pch", 19) %>%
			set("leaves_pch", 19) %>%
			set("leaves_cex", 1) %>%
			set("leaves_col", 4) %>%
			set("branches_lwd", 2) %>%
			set("branches_lwd", 2) %>%
			set("labels_cex", 1) %>%
			set("branches_k_color", value = c("darkorange2", "darkorange2", "forestgreen", "firebrick2", "darkorchid4"), k = 5) %>%
			plot(horiz = TRUE, main = "sapiens and chimps\nphylogeny",
				cex.axis=1.5, cex.main=1.5)
legend("topleft", legend=c("Pan troglodytes", "Homo (ancestors)", "H. sapiens neanderthalensis", "H. sapiens sapiens"),
	fill = c("darkorange2", "forestgreen", "firebrick2", "darkorchid4"), ins = 0.05, bty="n")
dev.off()